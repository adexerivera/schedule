/**
 * Application common javascript functionalities
 *
 * @author Adexe Rivera (adexerivera@gmail.com)
 */
/**
 * Manage week line fields
 */
function manageWeekline() {
    var weekLineConf = {
        mousedownSel: false,
        startDate: nextDay(0),
        dayLabels: ["L", "M", "X", "J", "V", "S", "D"],
        onChange: function () {
            $("#selectedDays").html(
                $(this).weekLine('getSelected')
            );
            $(this).find('.weekline-input').val($(this).weekLine('getSelected'))
        }
    };

    $(".weekline-input").each(function() {
        var parentWeekline = $(this).parent();
        if(!parentWeekline.hasClass('weekline')) {
            $(this).addClass('hidden');
            parentWeekline.addClass('weekline').weekLine(weekLineConf);
            parentWeekline.weekLine('setSelected', $(this).val());
        }
    });

    // Disable autocomplete on timepicker fields
    $('.timepicker').each(function() {
       $(this).attr("autocomplete", "off");
    });

    $('.timepicker').datetimepicker({
        datepicker: false,
        format: 'H:i',
        step  : 30
    });
}

$(function () {
    // Set datetimepicker class
    $.datetimepicker.setLocale('es');

    // Disable autocomplete on datepicker fields
    $(".datepicker").each(function() {
       $(this).attr("autocomplete", "off");
    });

    $('.datetimepicker').datetimepicker({
        step  : 30,
        format: 'd/m/Y H:i',
    });

    $('.datepicker').datetimepicker({
        format: 'd/m/Y',
        timepicker: false,
        scrollInput: false,
        dayOfWeekStart: 1
    });

    // Weekline
    manageWeekline();
    $('.field-collection-action').on('click', function(e) {
        manageWeekline();
    })

    // Select2 initialization
    $(".select2").select2();

    // Customer calendar view
    if($('#scheduleCalendarCustomer').length) {
        $('#scheduleCalendarCustomer').fullCalendar({
            lang: 'es',
            height: "parent",
            header: {
                left: '',
                center: 'title',
                right: ''
            },
            defaultView: 'listMonth',
            timeFormat: 'H:mm',
            eventSources: [
                {
                    url: '?entity=Schedule&action=print&month=9&year=2017&customer=73',
                    type: 'GET',
                    data: {
                        filter: {
                            customer: 73
                        }
                    },
                },
            ],
        });
    }
});
