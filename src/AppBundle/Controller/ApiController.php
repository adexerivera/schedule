<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use AppBundle\Entity\User;
use AppBundle\Entity\Event;
use AppBundle\Entity\Workspace;
use AppBundle\Entity\Token;
use AppBundle\Entity\Message;
use AppBundle\Entity\Difussion;
use AppBundle\Entity\Report;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use ADesigns\CalendarBundle\Entity\EventEntity;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use AppBundle\Event\ScheduleEvent;
use AppBundle\ScheduleEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Event\ReportEvent;
use AppBundle\ReportEvents;

class ApiController extends FOSRestController
{
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Post("/login")
     */
    public function loginAction(Request $request) {
        // Get login params
        $username = $request->get('username');
        $password = $request->get('password');
        $deviceId = $request->get('uuid');

        // Find user
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByUsername($username);
        if (!$user) {
            return new JsonResponse([
                'title' => 'Nombre de usuario o contraseña incorrecto'
            ]);
        }

        // Check if password is valid
        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);
        if (!$isValid) {
            return new JsonResponse([
                'title' => 'Nombre de usuario o contraseña incorrecto'
            ],
            404);
        }

        // Check if has movile access active
        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);
        if (!$user->getAppEnabled()) {
            return new JsonResponse([
                'title' => 'No tiene permisos para acceder' // You do not have permission to access
            ],
            404);
        }

        // Get and return token
        $token = $this->getToken($user);

        if($deviceId) {
            // Get device user token
            $deviceToken = $this->getDoctrine()
                ->getRepository(Token::class)
                ->findDeviceTokenForUser($user->getId(), $deviceId);
        }
        else {
            $deviceToken = null;
        }
        return new JsonResponse([
            'id' => $user->getId(),
            'token' => $token,
            'is_admin' => $user->hasRole('ROLE_ADMIN'),
            'username' => $user->getUsername(),
            'device_token' => $deviceToken,
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
            'surname' => $user->getSurname(),
            'reported' => $user->getReported(),
            'birthday' => ($user->getBirthday())? $user->getBirthday()->format('d/m/Y') : '',
            'timetable' => $user->getTimetable(),
            'anniversary' => ($user->getAnniversary())? $user->getAnniversary()->format('d/m/Y') : '',
        ]);
    }

    /**
     * Returns token for user.
     *
     * @param User $user
     * @return array
     */
    public function getToken(User $user) {
        return $this->container->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => $this->getTokenExpiryDateTime(),
            ]);
    }

    /**
     * Returns token expiration datetime.
     * @return string Unixtmestamp
     */
    private function getTokenExpiryDateTime() {
        $tokenTtl = $this->container->getParameter('lexik_jwt_authentication.token_ttl');
        $now = new \DateTime();
        $now->add(new \DateInterval('PT' . $tokenTtl . 'S'));
        return $now->format('U');
    }

    /**
     * @Post("/events")
     */
    public function loadEvents(Request $request) {
        // First of all check token
        // $this->checkToken($request->get('token'));
        // Get events params
        $iniDate = $request->get('start') . ' 00:00:00';
        $endDate = $request->get('end') . ' 23:59:59';

        $query = $this->entityManager->getRepository('AppBundle:Event')
            ->createQueryBuilder('event')
            ->where('event.session_start BETWEEN :startDate and :endDate')
            ->setParameter('startDate', $iniDate)
            ->setParameter('endDate', $endDate)
            ->orderBy('event.session_start', 'ASC');

        // Get customer filter
        $customer = $request->get('customer');
        if($customer != ''){
            $query->andWhere('event.customer = :customer')
                ->setParameter('customer', $customer);
        }

        // Get events
        $events = $query->getQuery()->getResult();

        $eventsData = [];
        foreach($events as $event) {
            // Set customer title
            $customerTitle = $event->getCustomer()->getUsername();
            if($customerTitle == $event->getCustomer()->getEmail()) {
                $customerTitle =$event->getCustomer()->getName();
            }
            $eventEntity = new EventEntity($customerTitle, $event->getSessionStart(), $event->getSessionEnd());
            $eventEntity->addField('id', $event->getId());
            $eventEntity->addField('type', 'training');
            $eventEntity->addField('customer_id', $event->getCustomer()->getId());
            $eventEntity->addField('attendance', $event->getAttendance());
            $eventObservation = ($event->getObservation())? $event->getObservation() : '';
            $eventEntity->addField('observation', $eventObservation);
            if($event->getCreated() != $event->getUpdated())
                $eventEntity->setBgColor('#F39C12');
            if($event->getType() == 'birthday') {
                $eventEntity->setTitle('Cumpleaños ' . $eventEntity->getTitle());
                $eventEntity->setBgColor('#E56A97');
                $eventEntity->setAllDay(true);
                $eventEntity->addField('type', 'birthday');
            }
            if($event->getType() == 'anniversary') {
                $eventEntity->setTitle('Aniversario ' . $eventEntity->getTitle());
                $eventEntity->setBgColor('#9795C6');
                $eventEntity->setAllDay(true);
                $eventEntity->addField('type', 'anniversary');
            }
            if($event->getSource() == 'manual') {
                $eventEntity->setBgColor('#59C593');
            }
            if(!$event->getAttendance()) {
                $eventEntity->setBgColor('#3A87AD');
            }
            array_push($eventsData, $eventEntity);
        }
        return ['eventsData' => $eventsData];
    }

    /**
     * @POST("/customers")
     */
    public function loadCustomers() {
        $customers = $this->getDoctrine()
            ->getRepository(User::class)
            ->findCustomer(['u.id', 'u.username']);
        return $customers;
    }

    /**
     * @Post("/event")
     */
    public function createOrUpdateEvent(Request $request) {
        // First get params
        $userId = $request->get('user_id');
        $eventId = $request->get('id');
        $duration = $request->get('duration');
        $startDate = $request->get('start_date');
        $startTime = $request->get('start_time');
        $customerId = $request->get('customer');
        $attendance = ($request->get('attendance') === 'true')? true : false;
        $observation = ($request->get('observation') === 'undefined')? '' : $request->get('observation');

        // Prepare session start and end
        $sessionStartStr = $startDate . ' ' . $startTime . ':00';
        $sessionStart = new \Datetime($sessionStartStr);
        $sessionEndStr = date('Y-m-d H:i:s', strtotime($sessionStartStr) + (60*$duration));
        $sessionEnd = new \Datetime($sessionEndStr);

        // Get creator or updater
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($userId);

        // Dispatch notification only if some data is updated
        $hasChangeOrIsNew = false;
        $sendNotification = false;
        if($eventId != 'undefined') {
            // Get event
            $em = $this->getDoctrine()->getEntityManager();
            $event = $this->getDoctrine()
                ->getRepository(Event::class)
                ->find($eventId);
            // Get previous date
            $eventPreviousStart = $event->getSessionStart();
            // Update event data
            if($eventPreviousStart != $sessionStart) {
                $event->setSessionStart($sessionStart);
                $hasChangeOrIsNew = true;
                $sendNotification = true;
            }
            if($event->getSessionEnd() != $sessionEnd) {
                $event->setSessionEnd($sessionEnd);
                $hasChangeOrIsNew = true;
                $sendNotification = true;
            }
            if($event->getAttendance() != $attendance) {
                $event->setAttendance($attendance);
                $hasChangeOrIsNew = true;
            }
            if($event->getObservation() != $observation) {
                $event->setObservation($observation);
                $hasChangeOrIsNew = true;
            }
            if($hasChangeOrIsNew) {
                $event->setUpdatedBy($user);
                $em->persist($event);
                $em->flush();
            }
            $eventDispatchingType = 'update';
        }
        else {
            // Get previous date
            $eventPreviousStart = date("Y-m-d H:i:s");
            $hasChangeOrIsNew = true;
            $sendNotification = true;
            // Get customer and workspace
            $customer = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneById($customerId);
            $workspace = $this->getDoctrine()
                ->getRepository(Workspace::class)
                ->findOneById(1);

            // Check if event already exist to prevent duplicate
            $eventExists = $this->getDoctrine()
                ->getRepository(Event::class)
                ->existEvent($customer->getId(), $sessionStart, $sessionEnd, 'training');
            if(!$eventExists) {
                $em = $this->getDoctrine()->getEntityManager();
                $event = new Event();
                $event->setCustomer($customer);
                $event->setWorkspace($workspace);
                $event->setName('Sesión');
                $event->setSessionStart($sessionStart);
                $event->setSessionEnd($sessionEnd);
                $event->setAttendance($attendance);
                $event->setObservation($observation);
                $event->setSource('manual');
                $event->setCreatedBy($user);
                $em->persist($event);
                $em->flush();
            }
            else {
                return new JsonResponse([
                    'message' => 'Ya existe un evento para ese día a esa hora',
                    'result' => false
                ]);
            }
            $eventDispatchingType = 'new';
        }

        // Dispatch Event
        if($sendNotification) {
            $scheduleEvent = new ScheduleEvent($event, $eventPreviousStart, $eventDispatchingType, 'mobile');
            $scheduleEvent = $this->container->get('event_dispatcher')->dispatch(ScheduleEvents::UPDATED, $scheduleEvent);
        }

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Post("/event-delete")
     */
    public function deleteEvent(Request $request) {
        // First get params
        $eventId = $request->get('id');
        $userId  = $request->get('user_id');
        // Get event
        $em = $this->getDoctrine()->getEntityManager();
        $event = $this->getDoctrine()
            ->getRepository(Event::class)
            ->find($eventId);

        // Get updater
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($userId);
        $event->setUpdatedBy($user);

        // Get previous date
        $eventPreviousStart = $event->getSessionStart();
        // Delete event
        try {
            $em->remove($event);
            $em->flush();

            // Dispatch Event
            $scheduleEvent = new ScheduleEvent($event, $eventPreviousStart, 'delete', 'mobile');
            $scheduleEvent = $this->container->get('event_dispatcher')->dispatch(ScheduleEvents::UPDATED, $scheduleEvent);

            return new JsonResponse([
                'result' => true,
            ]);
        } catch (ForeignKeyConstraintViolationException $e) {
            return new JsonResponse([
                'result' => false,
            ]);
        }
    }

    /**
     * @POST("/all-customers")
     */
    public function loadAllCustomers() {
        $customers = $this->getDoctrine()
            ->getRepository(User::class)
            ->findCustomer([
                'u.id',
                'u.name',
                'u.rate',
                'u.email',
                'u.phone',
                'u.active',
                'u.surname',
                'u.birthday',
                'u.reported',
                'u.username',
                'u.timetable',
                'u.appEnabled',
                'u.anniversary',
            ],
            0);
        return $customers;
    }

    /**
     * @Post("/customer")
     */
    public function createOrUpdateCustomer(Request $request) {
        // First get params
        $customerRequest = json_decode($request->get('customer'), true);

        // Check if customer id is passed for update user
        if(array_key_exists('id', $customerRequest) && $customerRequest['id'] != 'undefined') {
            // Get customer
            $em = $this->getDoctrine()->getEntityManager();
            $customer = $this->getDoctrine()
                ->getRepository(User::class)
                ->find($customerRequest['id']);
            // Update customer data
            $customer->setName($customerRequest['name']);
            $customer->setEmail($customerRequest['email']);
            $customer->setPhone($customerRequest['phone']);
            $customer->setActive($customerRequest['active']);
            $customer->setAppEnabled($customerRequest['appEnabled']);
            $customer->setReported($customerRequest['reported']);
            // Now optional
            if(array_key_exists('rate', $customerRequest))
                $customer->setRate($customerRequest['rate']);
            if(array_key_exists('surname', $customerRequest))
                $customer->setSurname($customerRequest['surname']);
            if(array_key_exists('birthday', $customerRequest))
                $customer->setBirthday(new \Datetime($customerRequest['birthday']));
            if(array_key_exists('timetable', $customerRequest))
                $customer->setTimetable($customerRequest['timetable']);
            else
                $customer->setTimetable([]);
            if(array_key_exists('anniversary', $customerRequest))
                $customer->setAnniversary(new \Datetime($customerRequest['anniversary']));
            $em->persist($customer);
            $em->flush();
        }
        else {
            // Get customer and workspace
            $em = $this->getDoctrine()->getEntityManager();
            $customer = new User();
            // First mandatory
            $customer->setName($customerRequest['name']);
            $customer->setEmail($customerRequest['email']);
            $customer->setPhone($customerRequest['phone']);
            $customer->setActive($customerRequest['active']);
            $customer->setReported($customerRequest['reported']);
            $customer->setUsername($customerRequest['username']);
            $customer->setPassword($this->get('security.password_encoder')->encodePassword($customer, $customerRequest['username']));
            // Now optional
            if(array_key_exists('rate', $customerRequest))
                $customer->setRate($customerRequest['rate']);
            if(array_key_exists('surname', $customerRequest))
                $customer->setSurname($customerRequest['surname']);
            if(array_key_exists('birthday', $customerRequest))
                $customer->setBirthday(new \Datetime($customerRequest['birthday']));
            if(array_key_exists('timetable', $customerRequest))
                $customer->setTimetable($customerRequest['timetable']);
            else
                $customer->setTimetable([]);
            if(array_key_exists('anniversary', $customerRequest))
                $customer->setAnniversary(new \Datetime($customerRequest['anniversary']));
            $em->persist($customer);
            $em->flush();
        }

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Post("/customer-delete")
     */
    public function deleteCustomer(Request $request) {
        // First get params
        $customerId = $request->get('id');
        // Get customer
        $em = $this->getDoctrine()->getEntityManager();
        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($customerId);
        // Delete event
        try {
            $em->remove($customer);
            $em->flush();
            return new JsonResponse([
                'result' => true,
            ]);
        } catch (ForeignKeyConstraintViolationException $e) {
            return new JsonResponse([
                'result' => false,
            ]);
        }
    }

    /**
     * @Post("/change-password")
     */
    public function changePassword(Request $request) {
        // First get params
        $passwordRequest = json_decode($request->get('form'), true);

        // Get user
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($passwordRequest['id']);

        $password = $passwordRequest['password'];
        $passwordRepeat = $passwordRequest['password_repeat'];

        if($password != '' && $passwordRepeat != '' && ($password == $passwordRepeat)) {
            $encoder = $this->container->get('security.password_encoder');
            $user->setPassword($this->get('security.password_encoder')->encodePassword($user, $passwordRequest['password']));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        else {
            return new JsonResponse([
                    'result' => false,
                    'title' => 'Las contraseñas no coinciden'
                ],
                404
            );
        }

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Post("/register-device")
     */
    public function registerDevice(Request $request) {
        // First get params
        $registerRequest = json_decode($request->get('register'), true);

        // Check if token exists and is active
        $existToken = $this->getDoctrine()
            ->getRepository(Token::class)
            ->existToken($registerRequest);

        if(empty($existToken)) {
            // First mark as inactive others tokens for this uuid
            $tokens = $this->getDoctrine()
                ->getRepository(Token::class)
                ->findTokenByDevice($registerRequest['uuid']);
            foreach ($tokens as $t) {
                $t->setActive(0);
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($t);
                $em->flush();
            }

            // Finally create uuid and token
            $token = new Token();
            $token->setToken($registerRequest['registrationid']);
            $token->setManufacturer($registerRequest['manufacturer']);
            $token->setModel($registerRequest['model']);
            $token->setPlatform($registerRequest['platform']);
            $token->setUuid($registerRequest['uuid']);
            $token->setVersion($registerRequest['version']);
            $token->setActive(1);
            if(array_key_exists('username', $registerRequest) && $registerRequest['username'] != '') {
                $customer = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneByUsername($registerRequest['username']);
                $token->setCustomer($customer);
            }
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($token);
            $em->flush();
        }
        else {
            if($existToken[0]->getCustomer() == null && (array_key_exists('username', $registerRequest) && $registerRequest['username'] != '')) {
                if($registerRequest['username'] != '') {
                    $customer = $this->getDoctrine()
                        ->getRepository(User::class)
                        ->findOneByUsername($registerRequest['username']);
                    $existToken[0]->setCustomer($customer);
                }
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($existToken[0]);
                $em->flush();
            }
        }

        return new JsonResponse([
            'token' => $registerRequest['registrationid'],
            'result' => true
        ]);
    }

    /**
     * @Post("/link-device")
     */
    public function linkDevice(Request $request) {
        // Get params
        $username = $request->get('username');
        $deviceId = $request->get('uuid');

        if($deviceId == 'null') {
            return new JsonResponse([
                    'result' => false,
                    'title' => 'No device uuid'
                ],
                404
            );
        }

        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByUsername($username);

        $token = $this->getDoctrine()
            ->getRepository(Token::class)
            ->findOneBy(['uuid' => $deviceId], ['id' => 'DESC']);

        $token->setCustomer($customer);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($token);
        $em->flush();

        return new JsonResponse([
            'token' => $token->getToken(),
            'result' => true
        ]);
    }

    /**
     * @Post("/user-info")
     */
    public function userInfo(Request $request) {
        // Get params
        $username = $request->get('username');

        // First get params
        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByUsername($username);

        $unreadNotifications = $this->getDoctrine()
            ->getRepository(Message::class)
            ->unreadMessages($customer, 'notification');

        $unreadReportsResult = $this->getUnreadReports($customer);
        if($customer->hasRole('ROLE_ADMIN')) {
            $unreadReports = 0;
            foreach ($unreadReportsResult['unreadReports'] as $value) {
                $unreadReports += $value;
            }
        }
        else {
            $unreadReports = $unreadReportsResult['unreadReports'];
        }

        return new JsonResponse([
            'unreadNotifications' => count($unreadNotifications),
            'unreadReports' => $unreadReports,
            'reported' => $customer->getReported()
        ]);
    }

    /**
     * @POST("/notifications")
     */
    public function loadNotifications(Request $request) {
        // Get params
        $username = $request->get('username');
        $page = $request->get('page');
        $type = ['notification', 'difussion'];

        // First get customer
        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByUsername($username);

        $notifications = $this->getDoctrine()
            ->getRepository(Message::class)
            ->userMessages($customer, $type, $page);

        return $notifications;
    }

    /**
     * @Post("/notification-viewed")
     */
    public function notificationReadEvent(Request $request) {
        // First get params
        $notificationRequest = json_decode($request->get('notification'), true);

        // Get notification
        $em = $this->getDoctrine()->getEntityManager();
        $notification = $this->getDoctrine()
            ->getRepository(Message::class)
            ->find($notificationRequest['id']);

        // Update notification read
        $notification->setViewed(new \Datetime($notificationRequest['viewed']));
        $em->persist($notification);
        $em->flush();

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Post("/notification-delete")
     */
    public function deleteNotification(Request $request) {
        // First get params
        $notificationId = $request->get('id');
        // Get customer
        $em = $this->getDoctrine()->getEntityManager();
        $notification = $this->getDoctrine()
            ->getRepository(Message::class)
            ->find($notificationId);

        // Update notification read
        $notification->setActive(false);
        $em->persist($notification);
        $em->flush();

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @POST("/mark-as-read-notifications")
     */
    public function markAsReadNotifications(Request $request) {
        // Get params
        $username = $request->get('username');
        $type = ['notification', 'difussion'];

        // First get customer
        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByUsername($username);

        $markAsViewedNum = $this->getDoctrine()
            ->getRepository(Message::class)
            ->markAsViewed($customer, $type);

        $notifications = $this->getDoctrine()
            ->getRepository(Message::class)
            ->userMessages($customer, $type);

        return $notifications;
    }

    /**
     * @Post("/test-notifications")
     */
    public function testNotifications(Request $request) {
        // Get params
        $token = $request->get('token');
        $platform = $request->get('platform');

        if($platform === 'ios') {
            $notification = new iOSMessage();
            $notification->setAPSContentAvailable(1);
            $notification->setMessage("API: Probando notification");
        } elseif ($platform === 'android') {
            $notification = new AndroidMessage();
            $notification->setFCM(true);
        }
        $notification->setDeviceIdentifier($token);
        $notification->setData([
            'type' => 'schedule',
            'message' => "API: Probando notification"
        ]);
        $this->container->get('rms_push_notifications')->send($notification);

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Get("/init-passwords")
     */
    public function initPasswords() {
        ini_set('max_execution_time', 300);
        // Get customers
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findCustomer();

        foreach ($users as $key => $user) {
            $encoder = $this->container->get('security.password_encoder');
            $user->setPassword($this->get('security.password_encoder')->encodePassword($user, $user->getUsername()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Get("/welcome-app")
     */
    public function welcomeApp() {
        // Get customers
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findCustomerAppEnabled();


        foreach ($users as $key => $user) {
            // Prepare message
            $message = (new \Swift_Message('Bienvenid@ ' . $user->getName() . ' a FORWARD 2.0'))
                ->setFrom(['forward@fwdss.com' => 'Forward'])
                ->setTo($user->getEmail())
                ->setBcc(['forward@fwdss.com', 'adexerivera@gmail.com'])
                ->setBody(
                    $this->renderView('emails/welcome_app.html.twig', [
                        'name' => $user->getName(),
                        'username' => $user->getUsername()
                    ]),
                    'text/html'
                );

            // Finally send mail
            $result = $this->get('mailer')->send($message);
        }

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Post("/support")
     */
    public function support(Request $request) {
        // Get params
        $supportRequest = json_decode($request->get('form'), true);

        // Prepare message
        $message = (new \Swift_Message('Plataforma de soporte de FWDSS'))
            ->setFrom(['forward@fwdss.com' => 'Forward'])
            ->setTo(['adexerivera@gmail.com', 'forward@fwdss.com'])
            ->setBody(
                $this->renderView('emails/support.html.twig', [
                    'email' => $supportRequest['email'],
                    'subject' => $supportRequest['subject'],
                    'description' => $supportRequest['description'],
                    'deviceInfo' => $supportRequest['deviceInfo']
                ]),
                'text/html'
            );

        // Finally send mail
        $result = $this->get('mailer')->send($message);

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Route("/accept_authorization")
     * @Method({"GET", "POST"})
     */
    public function acceptAuthorization(Request $request) {
        $authorizationToken = $request->get('token');

        $query = $this->entityManager->getRepository('AppBundle:Message')
            ->createQueryBuilder('message')
            ->where('message.authorization_token = :authorizationToken')
            ->setParameter('authorizationToken', $authorizationToken);

        // Get message
        $message = $query->getQuery()->setMaxResults(1)->getOneOrNullResult();

        if($message) {
            $em = $this->getDoctrine()->getEntityManager();
            $message->setAccepted(true);
            $message->setViewed(new \DateTime());
            $em->persist($message);
            $em->flush();
        }

        if ($request->isMethod('post')) {
            return new JsonResponse(['result' => true]);
        }
        else {
            return $this->redirectToRoute('accepted');
        }
    }

    /**
     * @Post("/check-gdpr")
     */
    public function checkGdpr(Request $request) {
        // Get params
        $authorizationId = $request->get('authorization_id');
        $customerId = $request->get('customer_id');

        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($customerId);

        $difussion = $this->getDoctrine()
            ->getRepository(Difussion::class)
            ->findOneById($authorizationId);

        $query = $this->entityManager->getRepository('AppBundle:Message')
            ->createQueryBuilder('m')
            ->where('m.customer = :customer')
            ->andWhere('m.difussion = :difussion')
            ->setParameter('customer', $customer)
            ->setParameter('difussion', $difussion);

        // Get gdpr
        $gdpr = $query->getQuery()->setMaxResults(1)->getOneOrNullResult();

        if($gdpr) {
            return new JsonResponse([
                'result' => true,
                'accepted' => $gdpr->getAccepted(),
                'title' => $gdpr->getTitle(),
                'body' => $gdpr->getBody(),
                'authorization_token' => $gdpr->getAuthorizationToken()
            ]);
        }
        else {
            return new JsonResponse([
                    'result' => false,
                    'title' => 'No authorization'
                ],
                404
            );

        }
    }

    /**
     * @POST("/reports")
     */
    public function loadReports(Request $request) {
        // Get params
        $userId = $request->get('user_id');
        $page = $request->get('page');

        // First get user
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($userId);

        // If user is admin load all reports else load user messages
        if($user->hasRole('ROLE_ADMIN')) {
            $trainers = $this->entityManager->getRepository(User::class)->findTrainer();
            $reports = $this->getDoctrine()
                ->getRepository(Report::class)
                ->loadReports(null, $trainers, $page);
            // Now get last send
            foreach($reports as $key => $report){
                $lastMessage = $this->getDoctrine()
                    ->getRepository(Report::class)
                    ->getLastMessage($report["client_id"]);
                $reports[$key]['last'] = $lastMessage["1"];
            }
            // Clean empty chats
            $reports = array_filter($reports, function($item){
                $notEmpty=count($item) == count(array_filter(array_map('trim', $item)));
                return $notEmpty;
            });
            // Sort by last message
            usort($reports, function ($item1, $item2) {
                return $item2['last'] <=> $item1['last'];
            });
            // Finally transform last on date time
            foreach($reports as $key => $report){
                $reports[$key]['last'] = new \Datetime($report['last']);
            }
        }
        else {
            $reports = $this->getDoctrine()
                ->getRepository(Report::class)
                ->loadReports($user, null, $page);
        }

        return $reports;
    }

    private function unique_key($array, $keyname){
        $new_array = array();
        foreach($array as $key=>$value){

            if(!isset($new_array[$value[$keyname]])){
                $new_array[$value[$keyname]] = $value;
            }
        }
        $new_array = array_values($new_array);
        return $new_array;
    }

    /**
     * @POST("/users-report")
     */
    public function loadUsersReport() {
        $trainers = $this->entityManager->getRepository(User::class)->findTrainer();
        $reports = $this->getDoctrine()
            ->getRepository(Report::class)
            ->usersReport($trainers);
        return $reports;
    }

    /**
     * @Post("/report-msg")
     */
    public function newReport(Request $request) {
        // Get params
        $msgRequest = json_decode($request->get('msg'), true);

        $sender = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($msgRequest["sender_id"]);

        $receiver = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($msgRequest["receiver_id"]);

        // Create report message
        $msg = new Report();
        // Set report client
        if($sender->hasRole('ROLE_ADMIN')) {
            $msg->setClient($receiver);
        } else {
            $msg->setClient($sender);
        }
        $msg->setSender($sender);
        $msg->setReceiver($receiver);
        $msg->setMessage($msgRequest["message"]);
        $msg->setImage($msgRequest["image"]);
        $sendDatetime = new \Datetime();
        $msg->setSend($sendDatetime);

        // Save message
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($msg);
        $em->flush();

        // Dispatch Event
        $reportEvent = new ReportEvent($msg, $sender, $receiver);
        $reportEvent = $this->container->get('event_dispatcher')->dispatch(ReportEvents::SEND, $reportEvent);

        return new JsonResponse([
            'result' => true,
            'send' => date_format($sendDatetime, "c")
        ]);
    }

    /**
     * @POST("/unread-reports")
     */
    public function unreadReports(Request $request) {
        // Get params
        $userId = $request->get('user_id');

        // First get user
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($userId);

        $result = $this->getUnreadReports($user);

        return new JsonResponse([
            'result' => $result
        ]);
    }

    private function getUnreadReports($user) {

        if($user->hasRole('ROLE_ADMIN')) {
            $trainers = $this->entityManager->getRepository(User::class)->findTrainer();
            $reports = $this->getDoctrine()
                ->getRepository(Report::class)
                ->loadReports(null, $trainers, 1, 999);
            $unreadReportsByCustomer = [];
            foreach ($reports as $key => $report) {
                $unreadReportsByCustomer[$report["client_id"]] = $this->getDoctrine()
                    ->getRepository(Report::class)
                    ->countUnreadReports($report["client_id"], $trainers);
            }
            $result = ["unreadReports" => $unreadReportsByCustomer];
        }
        else {
            $unreadReports = $this->getDoctrine()
                ->getRepository(Report::class)
                ->countUnreadReports($user, null);
            $result = ["unreadReports" => $unreadReports];
        }

        return $result;
    }

    /**
     * @POST("/mark-as-view-report-msg")
     */
    public function markAsViewReportMsg(Request $request) {
        // Get params
        $messages = $request->get('messages_id');
        $readerId = $request->get('reader_id');

        // First get reader
        $reader = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($readerId);


        // This is necesary for clean reports because receiver always is 88
        if($reader->hasRole('ROLE_ADMIN') && $readerId != '88') {
            $reader = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneById('88');
        }

        // Get messages identifiers
        if($messages == 'all') {
            $clientId = $request->get('user_id');

            // Now get client
            $client = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneById($clientId);

            if($client->hasRole('ROLE_ADMIN'))
                $client = null;
            $allMessages = $this->getDoctrine()
                ->getRepository(Report::class)
                ->unreadReports($reader, null, $client);

            $messagesId = [];
            foreach ($allMessages as $message) {
                $messagesId[] = $message['id'];
            }
        } else {
            $messagesId = explode(',', $messages);
        }

        // Mark as viewed
        $updated = $this->getDoctrine()
            ->getRepository(Report::class)
            ->markAsViewed($messagesId, $reader);

        return new JsonResponse([
            'result' => $updated
        ]);
    }

    /**
     * @POST("/get-image-for-report")
     */
    public function getImageForReport(Request $request) {
        // Get params
        $reportId = $request->get('report_id');

        // Mark as viewed
        $image = $this->getDoctrine()
            ->getRepository(Report::class)
            ->findImageById($reportId);

        return new JsonResponse([
            'image' => $image['image']
        ]);
    }

    /**
     * @POST("/report-gallery")
     */
    public function loadReportGallery(Request $request) {
        // Get params
        $userId = $request->get('user_id');
        $page = $request->get('page');

        // First get user
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($userId);

        // Get trainers
        $trainers = $this->entityManager->getRepository(User::class)->findTrainer();

        $gallery = $this->getDoctrine()
            ->getRepository(Report::class)
            ->userGallery($user->getId(), $trainers, $page);

        return $gallery;
    }

    /**
     * @Post("/remove-report")
     */
    public function removeReport(Request $request) {
        // Get params
        $messageId = $request->get('message_id');

        // Get report
        $msg = $this->getDoctrine()
            ->getRepository(Report::class)
            ->find($messageId);

        // Set erase report
        $msg->setErased(true);

        // Save message
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($msg);
        $em->flush();

        return new JsonResponse([
            'result' => true
        ]);
    }

    /**
     * @Post("/test-report-notification")
     */
    public function testReportNotification(Request $request) {

        // Get params
        $senderId = $request->get('sender_id');
        $receiverId = $request->get('receiver_id');

        $sender = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($senderId);

        $receiver = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($receiverId);

        // Create report message
        $msg = new Report();
        $msg->setSender($sender);
        $msg->setReceiver($receiver);
        $msg->setMessage("Test report notification");
        $msg->setSend(new \Datetime());

        // Dispatch Event
        $reportEvent = new ReportEvent($msg, $sender, $receiver);
        $reportEvent = $this->container->get('event_dispatcher')->dispatch(ReportEvents::SEND, $reportEvent);

        return new JsonResponse([
            'result' => true,
        ]);
    }

}
