<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Event as Session;

class ScheduleEvent extends Event
{
    private $session;

    /**
     * @var \DateTime
     */
    private $previousStartSession;

    /**
     * @var String
     */
    private $type;

    /**
     * @var String
     */
    private $source;

    /**
     * Create schedule event for events
     * @param Session   $session              Session
     * @param DateTime  $previousStartSession Previous start date time
     * @param String    $type                 Event type new, update or delete
     * @param String    $source               Event source web or mobile
     */

    public function __construct(Session $session, $previousStartSession, $type, $source) {
        $this->session = $session;
        $this->previousStartSession = $previousStartSession;
        $this->type = $type;
        $this->source = $source;
    }

    public function getSession() {
        return $this->session;
    }

    public function getPreviousStartSession() {
        return $this->previousStartSession;
    }

    public function getType(){
        return $this->type;
    }

    public function getSource(){
        return $this->source;
    }
}
