<?php

namespace AppBundle\EventListener;

use ADesigns\CalendarBundle\Event\CalendarEvent;
use ADesigns\CalendarBundle\Entity\EventEntity;
use Doctrine\ORM\EntityManager;

class CalendarEventListener
{
	private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	public function loadEvents(CalendarEvent $calendarEvent)
	{
		$startDate = $calendarEvent->getStartDatetime();
		$endDate = $calendarEvent->getEndDatetime();

     	$request = $calendarEvent->getRequest();

		$query = $this->entityManager->getRepository('AppBundle:Event')
            ->createQueryBuilder('event')
            ->where('event.session_start BETWEEN :startDate and :endDate')
            ->setParameter('startDate', $startDate->format('Y-m-d 00:00:00'))
            ->setParameter('endDate', $endDate->format('Y-m-d 23:59:59'));

        // Get filters
        $filter = $request->get('filter');
        // Add customer filter
        if($filter['customer'] != ''){
            $query->andWhere('event.customer = :customer')
                ->setParameter('customer', $filter['customer']);
        }

        // Get events
        $events = $query->getQuery()->getResult();

		foreach($events as $event) {
            // Set customer title
            $customerTitle = $event->getCustomer()->getUsername();
            if($customerTitle == $event->getCustomer()->getEmail()) {
                $customerTitle =$event->getCustomer()->getName();
            }
	    	$eventEntity = new EventEntity($customerTitle, $event->getSessionStart(), $event->getSessionEnd());
            if($event->getCreated() != $event->getUpdated())
                $eventEntity->setBgColor('#F39C12');
            if($event->getType() == 'birthday') {
                $eventAge = $event->getCustomer()->getAge($event->getSessionStart());
                if($eventAge == '1')
                    $eventAge .= 'er ';
                else
                    $eventAge .= 'º ';
                $eventEntity->setTitle($eventAge . 'Cumpleaños ' . $eventEntity->getTitle());
                $eventEntity->setBgColor('#E56A97');
                $eventEntity->setAllDay(true);
            }
            if($event->getType() == 'anniversary') {
                $eventAge = $event->getCustomer()->getAnniversaryAge($event->getSessionStart());
                if($eventAge == '1')
                    $eventAge .= 'er ';
                else
                    $eventAge .= 'º ';
                $eventEntity->setTitle($eventAge . 'Aniversario ' . $eventEntity->getTitle());
                $eventEntity->setBgColor('#9795C6');
                $eventEntity->setAllDay(true);
            }
            if($event->getSource() == 'manual') {
                $eventEntity->setBgColor('#59C593');
            }
            if(!$event->getAttendance()) {
                $eventEntity->setBgColor('#3A87AD');
            }
            $eventEntity->setCssClass('fwdss_session_' . $event->getId());
		    // Finally, add the event to the CalendarEvent for displaying on the calendar
		    $calendarEvent->addEvent($eventEntity);
		}
	}
}
