<?php

namespace AppBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Diary.
 *
 * @author Adexe Rivera
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiaryRepository")
 *
 * @ORM\Table(name="diary", indexes={@ORM\Index(name="search_idx", columns={"customer_id", "month", "year"})})
 */
class Diary
{
    /**
     * The identifier of the event.
     *
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id = null;

    /**
     * The diary observations.
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * The customer for diary.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="diaries")
     */
    protected $customer;

    /**
     * The customer username.
     *
     * @var string
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $username;

    /**
     * The diary month.
     *
     * @var string
     * @ORM\Column(type="string", length=2)
     */
    private $month;

    /**
     * The diary year.
     *
     * @var string
     * @ORM\Column(type="string", length=4)
     */
    private $year;

    /**
     * The diary number of sessions.
     *
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sessions;

    /**
     * The diary rate.
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $rate;

    /**
     * @var \boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sent;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $send_date;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var \boolean
     * @ORM\Column(type="boolean")
     */
    private $replanned;

    public function __construct()
    {
        $this->sent = false;
        $this->replanned = false;
    }

    /**
     * Get the id of the event.
     * Return null if the event is new and not saved.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the observations of the diary.
     *
     * @param string $observations
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;
    }

    /**
     * Get the observations of the diary.
     *
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * @param User $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return User
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set the username of the diary.
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get the username of the diary.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the month of the diary.
     *
     * @param string $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * Get the month of the diary.
     *
     * @return string
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set the year of the diary.
     *
     * @param string $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get the year of the diary.
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the sessions of the diary.
     *
     * @param int $sessions
     */
    public function setSessions($sessions)
    {
        $this->sessions = $sessions;
    }

    /**
     * Get the sessions of the diary.
     *
     * @return int
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set the rate of the diary.
     *
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * Get the rate of the diary.
     *
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set the send date of the diary.
     *
     * @param datetime $sendDate
     */
    public function setSendDate($sendDate)
    {
        $this->send_date = $sendDate;
    }

    /**
     * Get the send date of the diary.
     *
     * @return datetime
     */
    public function getSendDate()
    {
        return $this->send_date;
    }

    /**
     * Get the created date of the diary.
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get the updated date of the diary.
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set the sent of the diary.
     *
     * @param boolean $sent
     */
    public function setSent($sent)
    {
        $this->sent = $sent;
    }

    /**
     * Get the sent of the diary.
     *
     * @return boolean
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set the replanned of the diary.
     *
     * @param boolean $replanned
     */
    public function setReplanned($replanned)
    {
        $this->replanned = $replanned;
    }

    /**
     * Get the replanned of the diary.
     *
     * @return boolean
     */
    public function getReplanned()
    {
        return $this->replanned;
    }

    /**
     * Get user full name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->customer->getFullName();
    }
}
