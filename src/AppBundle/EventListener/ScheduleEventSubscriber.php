<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Event\ScheduleEvent;
use AppBundle\ScheduleEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Token;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Entity\Diary;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;

class ScheduleEventSubscriber implements EventSubscriberInterface
{
	private $entityManager;
	private $container;

	public function __construct(EntityManager $entityManager, ContainerInterface $container) {
		$this->entityManager = $entityManager;
        $this->container = $container;
	}

    /**
     * @return array
     */
    public static function getSubscribedEvents() {
        return array(
            ScheduleEvents::UPDATED => 'onScheduleUpdate',
        );
    }

    /**
     * On schedule session updated
     * @param  ScheduleEvent $session Schedule session
     */
    public function onScheduleUpdate(ScheduleEvent $session) {
        // First check if session has diary to notify
        $month = $session->getSession()->getSessionStart()->format('n');
        $year = $session->getSession()->getSessionStart()->format('Y');
        $diaryExists = $this->entityManager
            ->getRepository(Diary::class)
            ->existDiary($session->getSession()->getCustomer()->getId(), $month, $year);

        // If diary not exist then not send event
        if(!$diaryExists) {
            return new JsonResponse([
                'result' => true,
            ]);
        }

        $users = [];
        // Get trainers
        $trainers = $this->entityManager->getRepository(User::class)->findTrainer();
        $ignoreAdmin = false;
        if($session->getSource() == 'mobile') {
            $ignoreAdmin = true;
            $ignoreAdminId = ($session->getType() == 'new')? $session->getSession()->getCreatedBy()->getId() : $session->getSession()->getUpdatedBy()->getId();
        }
        foreach ($trainers as $trainer) {
            if($ignoreAdmin && $trainer->getId() == $ignoreAdminId)
                continue;
            $users[] = $trainer->getId();
        }
        $users[] = $session->getSession()->getCustomer()->getId();
        // Get active tokens for users
        $tokens = $this->entityManager->getRepository(Token::class)->findTokensByUsers($users);

        $messageTitle = '';
        $notificationMessage = '';
        $messageUserSaved = [];
        foreach ($tokens as $key => $token) {
            if(strtolower($token->getPlatform()) === 'ios') {
                $notification = new iOSMessage();
            } elseif (strtolower($token->getPlatform()) === 'android') {
                $notification = new AndroidMessage();
                $notification->setFCM(true);
            }

            // @TODO create alias to change this
            if($session->getType() == 'new')
                $creatorModifier = $session->getSession()->getCreatedBy()->getUsername();
            else
                $creatorModifier = $session->getSession()->getUpdatedBy()->getUsername();
            if($creatorModifier == 'rdearmas')
                $creatorModifier = 'Ruymán';

            // Prepare messages
            switch ($session->getType()) {
                case 'new':
                    $messageTitle = 'Nueva sesión';
                    $notificationMessage = ucfirst($creatorModifier) . ' ha creado una sesión para el ' . strftime('%d/%m/%Y a las %H:%M', $session->getSession()->getSessionStart()->getTimestamp());
                    break;
                case 'update':
                    $messageTitle = 'Cambio de sesión';
                    $notificationMessage = ucfirst($creatorModifier) . ' ha cambiado la sesión del ' . strftime('%d/%m/%Y a las %H:%M', $session->getPreviousStartSession()->getTimestamp());
                    $notificationMessage .= ' ha sido movida para el ' . strftime('%d/%m/%Y a las %H:%M', $session->getSession()->getSessionStart()->getTimestamp());
                    break;
                case 'delete':
                    $messageTitle = 'Sesión borrada';
                    $notificationMessage = ucfirst($creatorModifier) . ' ha borrado la sesión del ' . strftime('%d/%m/%Y a las %H:%M', $session->getPreviousStartSession()->getTimestamp());
                    break;
            }
            if($token->getCustomer()->hasRole('ROLE_ADMIN')) {
                $messageTitle .= ' de ' . $session->getSession()->getCustomer()->getUsername();
                $notificationMessage = $messageTitle . '. ' . $notificationMessage;
            }

            $notification->setMessage($notificationMessage);
            $notification->setDeviceIdentifier($token->getToken());
            $notification->setData([
                'type' => 'schedule',
                'message' => $notificationMessage
            ]);
            $this->container->get('rms_push_notifications')->send($notification);

            // Now save message only once time by user not by token
            if(!in_array($token->getCustomer()->getId(), $messageUserSaved)) {
                $message = new Message();
                $message->setTitle($messageTitle);
                $message->setBody($notificationMessage);
                $message->setCustomer($token->getCustomer());
                $message->setSend(new \DateTime());
                $this->entityManager->persist($message);
                $this->entityManager->flush();
                $messageUserSaved[] = $token->getCustomer()->getId();
            }
        }

        // If ignore admin not send notification but save message
        if($ignoreAdmin) {
            $trainerIgnored = ($session->getType() == 'new')? $session->getSession()->getCreatedBy() : $session->getSession()->getUpdatedBy();
            $message = new Message();
            $message->setTitle($messageTitle);
            $message->setBody($notificationMessage);
            $message->setCustomer($trainerIgnored);
            $message->setSend(new \DateTime());
            $this->entityManager->persist($message);
            $this->entityManager->flush();
        }

        return new JsonResponse([
            'result' => true,
        ]);

    }
}
