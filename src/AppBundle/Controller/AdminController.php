<?php

namespace AppBundle\Controller;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Diary;
use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use AppBundle\Entity\Token;
use AppBundle\Entity\Message;
use Doctrine\ORM\Tools\Pagination\Paginator;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;

class AdminController extends EasyAdminController
{
    /**
     * Send diary to user or users
     */
    public function sendAction() {
        $id = $this->request->get('id');

        if($id != '0') {
            $diary = $this->em->getRepository('AppBundle:Diary')->find($id);
            $this->sendDiary($diary);
            // Redirect to the 'list' view of the given entity
            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));
        }
        else {
            $month = $this->request->get('month');
            $year = $this->request->get('year');
            $diaries = $this->getDoctrine()
                ->getRepository(Diary::class)
                ->diariesMonth($month, $year);
            $count = 0;
            foreach ($diaries as $diary) {
                $this->sendDiary($diary);
            }
            return $this->json(array('result' => true));
        }
    }

    /**
     * Send diary functinality
     * @param  [object] $diary Diary to send
     */
    public function sendDiary($diary) {
        $spanishMonths = [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ];
        $monthName = $spanishMonths[$diary->getMonth() - 1];
        $iniDate =  date('Y-m-01 00:00:00', strtotime(date($diary->getYear() . '-' . intval($diary->getMonth()) . '-1')));
        $endDate =  date('Y-m-t 23:59:59', strtotime(date($diary->getYear() . '-' . intval($diary->getMonth()) . '-1')));

        // Get events
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->eventsCustomerMonth($diary->getCustomer()->getId(), $iniDate, $endDate, 'training');

        // Render pdf
        $html = $this->renderView('easy_admin/schedule/pdf.html.twig', [
            'rate'   => $diary->getRate(),
            'events' => $events,
            'observations' => $diary->getObservations()
        ]);
        $header = $this->renderView('easy_admin/schedule/pdf_header.html.twig', [
            'iniDate' => $iniDate,
            'customer' => $diary->getCustomer()
        ]);
        $footer = $this->renderView('easy_admin/schedule/pdf_footer.html.twig');
        $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html, [
            'header-html' => $header,
            'footer-html' => $footer
        ]);

        // Prepare message
        $message = (new \Swift_Message('Plan ' . strtolower($monthName) . ' ' . $diary->getYear() . ' ' . $diary->getCustomer()->getName()))
            ->setFrom(['forward@fwdss.com' => 'Forward'])
            ->setTo($diary->getCustomer()->getEmail())
            ->setBcc(['forward@fwdss.com'])
            ->setBody(
                $this->renderView('emails/sessions.html.twig', [
                    'month' => $monthName
                ]),
                'text/html'
            );

        // Prepare filename
        $filename = sprintf(
            '%s_%s.pdf',
            preg_replace('/\s+/', '_', $diary->getCustomer()->getName()),
            date('m_Y', strtotime(date($diary->getYear() . '-' . intval($diary->getMonth()) . '-1')))
        );

        // Prepare attach
        $attach = \Swift_Attachment::newInstance(
            $pdf,
            $filename,
            'application/pdf'
        );
        $message->attach($attach);

        // Finally send mail
        $result = $this->get('mailer')->send($message);
        if($result) {
            $diary->setSendDate(new \DateTime());
            $diary->setSent(true);
            $diary->setReplanned(false);
            $this->em->persist($diary);
            $this->em->flush();
        }
    }

    /**
     * The method that is executed when the user performs a 'list' action on an entity.
     *
     * @return Response
     */
    protected function listDiaryAction() {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, array('paginator' => $paginator));

        return $this->render($this->entity['templates']['list'], array(
            'paginator' => $paginator,
            'fields' => $fields,
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ));
    }

    /**
     * The method that is executed when the user performs a query on diary.
     *
     * @return Response
     */
    protected function searchDiaryAction() {
        // if the search query is empty, redirect to the 'list' action
        if ('' === $this->request->query->get('query') && '' === $this->request->query->get('month') && '' === $this->request->query->get('year')) {
            $queryParameters = array_replace($this->request->query->all(), array('action' => 'list', 'query' => null));
            $queryParameters = array_filter($queryParameters);

            return $this->redirect($this->get('router')->generate('easyadmin', $queryParameters));
        }

        if ($this->request->query->get('month') != null || $this->request->query->get('year') != null) {
            $page = $this->request->query->get('page', 1);
            $maxResults = 500;
            $query = $this->em->createQueryBuilder();
            $query->select('d')
                ->from('AppBundle:Diary', 'd')
                ->setFirstResult($page)
                ->setMaxResults($maxResults)
                ;

            if('' !== $this->request->query->get('query'))
                $query->where('d.id = :id')
                    ->orWhere('LOWER(d.username) LIKE ' . "'%" . $this->request->query->get('query') . "%'")
                    ->orWhere('LOWER(d.observations) LIKE ' . "'%" . $this->request->query->get('query') . "%'")
                    ->orWhere("LOWER(d.observations) IN ('" . $this->request->query->get('query') . "')")
                    ->orWhere('d.sessions = :sessions')
                    ->orWhere('d.rate = :rate')
                    ->setParameter('id', $this->request->query->get('query'))
                    ->setParameter('sessions', $this->request->query->get('query'))
                    ->setParameter('rate', $this->request->query->get('query'));

            if('' !== $this->request->query->get('month'))
                $query->andWhere('d.month = :month')->setParameter('month', $this->request->query->get('month'));

            if('' !== $this->request->query->get('year'))
                $query->andWhere('d.year = :year')->setParameter('year', $this->request->query->get('year'));

            $paginator = $this->get('easyadmin.paginator')->createOrmPaginator($query, $page, $maxResults);
        }
        else {
            $searchableFields = $this->entity['search']['fields'];
            $paginator = $this->findBy($this->entity['class'], $this->request->query->get('query'), $searchableFields, $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['search']['dql_filter']);
        }

        $fields = $this->entity['list']['fields'];

        return $this->render($this->entity['templates']['list'], array(
            'fields' => $fields,
            'paginator' => $paginator,
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ));
    }

    protected function editDiaryAction() {
        $this->dispatch(EasyAdminEvents::PRE_EDIT);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
            $newValue = 'true' === mb_strtolower($this->request->query->get('newValue'));
            // If property is replanned then send when new value its true else functinality by default
            if($property == 'sent') {
                if($newValue == 'true') {
                    $diary = $this->em->getRepository('AppBundle:Diary')->find($id);
                    $this->sendDiary($diary);
                }
            }
            else {
                $fieldsMetadata = $this->entity['list']['fields'];

                if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
                    throw new \RuntimeException(sprintf('The type of the "%s" property is not "toggle".', $property));
                }

                $this->updateEntityProperty($entity, $property, $newValue);
            }

            return new Response((string) $newValue);
        }

        $fields = $this->entity['edit']['fields'];

        $editForm = $this->executeDynamicMethod('create<EntityName>EditForm', array($entity, $fields));
        $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

        $editForm->handleRequest($this->request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->dispatch(EasyAdminEvents::PRE_UPDATE, array('entity' => $entity));

            $this->executeDynamicMethod('preUpdate<EntityName>Entity', array($entity));
            $this->em->flush();

            $this->dispatch(EasyAdminEvents::POST_UPDATE, array('entity' => $entity));

            return $this->redirectToReferrer();
        }

        $this->dispatch(EasyAdminEvents::POST_EDIT);

        return $this->render($this->entity['templates']['edit'], array(
            'form' => $editForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * The method that is executed when the user performs a 'show' action on diary.
     *
     * @return Response
     */
    protected function showDiaryAction(){
        $this->dispatch(EasyAdminEvents::PRE_SHOW);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        $fields = $this->entity['show']['fields'];
        $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

        $this->dispatch(EasyAdminEvents::POST_SHOW, array(
            'deleteForm' => $deleteForm,
            'fields' => $fields,
            'entity' => $entity,
        ));

        // Get events
        $iniDate =  date('Y-m-01 00:00:00', strtotime(date($entity->getYear() . '-' . intval($entity->getMonth()) . '-1')));
        $endDate =  date('Y-m-t 23:59:59', strtotime(date($entity->getYear() . '-' . intval($entity->getMonth()) . '-1')));
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->eventsCustomerMonth($entity->getCustomer()->getId(), $iniDate, $endDate, 'training');

        return $this->render($this->entity['templates']['show'], array(
            'entity' => $entity,
            'fields' => $fields,
            'delete_form' => $deleteForm->createView(),
            'events' => $events
        ));
    }

    /**
     * Change user password.
     */
    public function changePasswordAction() {
        $request = $this->request;
        $id = $this->request->query->get('id');
        $password = $request->get('user')['password'];
        $passwordRepeat = $request->get('user')['password_repeat'];
        $user = $this->em->getRepository(User::class)->findOneById($id);

        if($password != '' && $passwordRepeat != '' && ($password == $passwordRepeat)) {
            $encoder = $this->container->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $password));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            // Redirect to the 'list' view of the given entity
            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));
        }
        else {
            return $this->render('easy_admin/User/change_password.html.twig', [
                'user' => $user,
            ]);
        }
    }

    /**
     * Send difussion to users
     */
    public function broadcastAction() {
        $id = $this->request->get('id');

        if($id != '0') {
            $difussion = $this->em->getRepository('AppBundle:Difussion')->find($id);
            if($difussion->getType() == 'notification')
                $this->sendDifussion($difussion);
            else
                $this->sendAuthorization($difussion);
            // Redirect to the 'list' view of the given entity
            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));
        }
    }

    /**
     * Send difussion functinality
     * @param  [object] $difussion Difussion to send
     */
    public function sendDifussion($difussion) {
        // Get active tokens
        $tokens = $this->em->getRepository(Token::class)->findTokens();

        $count = 0;
        foreach ($tokens as $key => $token) {
            if(strtolower($token->getPlatform()) === 'ios') {
                $notification = new iOSMessage();
            } elseif (strtolower($token->getPlatform()) === 'android') {
                $notification = new AndroidMessage();
                $notification->setFCM(true);
            }
            $notification->setMessage($difussion->getBody());
            $notification->setDeviceIdentifier($token->getToken());
            $notification->setData([
                'message' => $difussion->getBody()
            ]);
            $this->container->get('rms_push_notifications')->send($notification);

            // Now save message
            $message = new Message();
            $message->setTitle($difussion->getTitle());
            $message->setBody($difussion->getBody());
            $message->setCustomer($token->getCustomer());
            $message->setType('difussion');
            $message->setSend(new \DateTime());
            $this->em->persist($message);
            $this->em->flush();
            $count++;
        }
        $difussion->setSend(new \DateTime());
        $difussion->setCount($count);
        $this->em->persist($difussion);
        $this->em->flush();
    }

    /**
     * Send difussion authorization mail
     * @param  [object] $difussion Difussion to send
     */
    public function sendAuthorization($difussion, $customers = null) {

        if(!$customers) {
            $customers = $this->getDoctrine()
                ->getRepository(User::class)
                ->findAll();
        }

        $count = $difussion->getCount();
        foreach ($customers as $key => $customer) {

            $alreadySend = $this->getDoctrine()
                ->getRepository(Message::class)
                ->difussionSend($customer, $difussion);

            if(!$alreadySend) {
                // Generate authorization token
                $authorizationToken = md5(uniqid($customer->getId() . $customer->getUsername(), true));

                // Now save message
                $message = new Message();
                $message->setTitle($difussion->getTitle());
                $message->setBody($difussion->getBody());
                $message->setCustomer($customer);
                $message->setType($difussion->getType());
                $message->setSend(new \DateTime());
                $message->setDifussion($difussion);
                $message->setAuthorizationToken($authorizationToken);
                $this->em->persist($message);
                $this->em->flush();

                // Prepare message
                $mail = (new \Swift_Message('Autorización ' . $difussion->getTitle()))
                    ->setFrom(['forward@fwdss.com' => 'Forward'])
                    ->setTo($customer->getEmail())
                    ->setBcc(['forward@fwdss.com', 'adexerivera@gmail.com'])
                    ->setBody(
                        $this->renderView('emails/authorization.html.twig', [
                            'authorizationToken' => $authorizationToken,
                            'username' => $customer->getUsername(),
                            'body' => $difussion->getBody()
                        ]),
                        'text/html'
                    );

                // Finally send mail
                $result = $this->get('mailer')->send($mail);
                $count++;
            }
        }

        $difussion->setSend(new \DateTime());
        $difussion->setCount($count);
        $this->em->persist($difussion);
        $this->em->flush();
    }

    /**
     * Send welcome message to user
     */
    public function welcomeAction() {
        $id = $this->request->get('id');
        // Get user
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($id);
        
        // Prepare message
        $message = (new \Swift_Message('Bienvenid@ ' . $user->getName() . ' a FORWARD 2.0'))
            ->setFrom(['forward@fwdss.com' => 'Forward'])
            ->setTo($user->getEmail())
            ->setBcc('forward@fwdss.com')
            ->setBody(
                $this->renderView('emails/welcome_app.html.twig', [
                    'name' => $user->getName(),
                    'username' => $user->getUsername()
                ]),
                'text/html'
            );

        // Finally send mail
        $result = $this->get('mailer')->send($message);

        // Now send LOPD authorization
        $difussion = $this->em->getRepository('AppBundle:Difussion')->find(3);
        $this->sendAuthorization($difussion, [$user]);

        // Redirect to the 'list' view of the given entity
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));
    }


    /**
     * The method that is executed when the user performs a 'show' action on difussion.
     *
     * @return Response
     */
    protected function showDifussionAction(){
        $this->dispatch(EasyAdminEvents::PRE_SHOW);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        $fields = $this->entity['show']['fields'];
        $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

        $this->dispatch(EasyAdminEvents::POST_SHOW, array(
            'deleteForm' => $deleteForm,
            'fields' => $fields,
            'entity' => $entity,
        ));

        // Get authorizations
        $authorizations = $this->em->getRepository('AppBundle:Message')
            ->createQueryBuilder('m')
            ->where('m.difussion = :difussion')
            ->setParameter('difussion', $id);

        return $this->render($this->entity['templates']['show'], array(
            'entity' => $entity,
            'fields' => $fields,
            'type' => $fields['type'],
            'delete_form' => $deleteForm->createView(),
            'authorizations' => $authorizations->getQuery()->getResult()
        ));
    }

}
