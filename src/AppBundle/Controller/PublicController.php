<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PublicController extends FOSRestController
{

    /**
     * @Route("/accepted", name="accepted")
     */
    public function acceptedAction() {
        return $this->render('easy_admin/User/accepted.html.twig');
    }

    /**
     * @Route("/legal-notice", name="legal-notice")
     */
    public function legalNoticeAction() {
        return $this->render('easy_admin/Public/legal_notice.html.twig');
    }

}
