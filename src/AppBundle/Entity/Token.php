<?php

namespace AppBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Token.
 *
 * @author Adexe Rivera
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TokenRepository")
 *
 * @ORM\Table(name="token")
 */
class Token
{
    /**
     * The identifier of the token.
     *
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id = null;

    /**
     * Token.
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * The customer for token.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tokens")
     */
    protected $customer;

    /**
     * Manufacturer.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    public $manufacturer;

    /**
     * Model.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    public $model;

    /**
     * Platform.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    public $platform;

    /**
     * UUID.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    public $uuid;

    /**
     * Version.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    public $version;

    /**
     * The workspace active.
     *
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * Get the id of the token.
     * Return null if the token is new and not saved.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the token.
     *
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Get the token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param User $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return User
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set the manufacturer.
     *
     * @param string $manufacturer
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * Get the manufacturer.
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set the model.
     *
     * @param string $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Get the model.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the platform.
     *
     * @param string $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    /**
     * Get the platform.
     *
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Set the uuid.
     *
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Get the uuid.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set the version.
     *
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * Get the version.
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set the active of the token.
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get the active of the token.
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get the created date of the token.
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get the updated date of the token.
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
