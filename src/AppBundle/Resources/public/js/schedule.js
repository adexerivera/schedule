/**
 * Schedule javascript functionalities
 *
 * @author Adexe Rivera (adexerivera@gmail.com)
 */
/**
 * Get filters
 * @return {object} Filter object
 */
var selectedSession;

function getFilters() {
    var filter = {
        customer: ''
    };
    // Get customer filter
    if($("#customFilter").val()) {
        filter.customer = $("#customFilter").val();
    };
    return filter;
}

/**
 * Update event management
 * @param  {object} event      Event to update
 * @param  {string} revertFunc Function call on cancel
 */
function updateEvent(event, revertFunc) {
    swal({
        type: 'warning',
        text: 'que quieres mover esta sesión para el ' + event.start.format('LLLL'),
        title: '¿Estás segur@?',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si estoy segur@!',
        confirmButtonColor: '#3085d6',
        input: 'checkbox',
        inputValue: 0,
        inputPlaceholder: 'Notificar por móvil',
    }).then(function () {
        $.ajax({
            type: 'POST',
            url: '?entity=Schedule&action=update',
            data: {
                end: event.end.format('Y-MM-DD HH:mm:ss'),
                start: event.start.format('Y-MM-DD HH:mm:ss'),
                notify: $('#swal2-checkbox').is(':checked')? 1 : 0,
                eventId: event.className[0].replace("fwdss_session_", ""),
            },
            error: function( jqXhr, textStatus, errorThrown ){
                swal({
                    type: 'error',
                    text: 'A ocurrido un error en la actualización de la sesión. Cotacte con el administrador',
                    title: 'Ooops ...',
                    timer: 1000
                });
                console.log( errorThrown );
            }
        });
    },
    function (dismiss) {
        if (dismiss === 'timer')
            console.log('I was closed by the timer')
        if (dismiss === 'cancel') {
            console.log('Cancel by user');
            revertFunc();
        }
    });
}

/**
 * Delete event management
 * @param  {object} event      Event to detele
 */
function deleteEvent(event) {
    swal({
        type: 'warning',
        text: 'Borrar la sesion de ' + event.title + ' del día ' + event.start.format('LLLL'),
        title: '¿Estás segur@?',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        input: 'checkbox',
        inputValue: 0,
        inputPlaceholder: 'Notificar por móvil',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '?entity=Schedule&action=deleteEvent',
                    data: {
                        eventId: event.className[0].replace("fwdss_session_", ""),
                        notify: $('#swal2-checkbox').is(':checked')? 1 : 0,
                    },
                    success: function( data, textStatus, jQxhr ){
                        if(data.result)
                            resolve();
                        else
                            swal("Ooops!", "El borrado ha fallado!", "error");
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        swal("Ooops!", "El borrado ha fallado!", "error");
                    }
                })
            })
        },
        allowOutsideClick: false
    }).then(function() {
        swal({
            type: 'success',
            title: 'Sesión borrada!',
            timer: 1000
        });
        $('#scheduleCalendar').fullCalendar('refetchEvents');
        $('#customFilter').trigger('change');
        selectedSession = null;
        $('#newSessionModal').modal('hide');
    }).catch(function() {
        console.log('Cancel by user');
    });
}

function editEvent(event) {
    selectedSession = event;
    $.ajax({
        type: 'POST',
        url: '?entity=Schedule&action=getEvent',
        data: {
            eventId: event.className[0].replace("fwdss_session_", "")
        },
        success: function( data, textStatus, jQxhr ){
            if(data.id != null) {
                let startDateLocale = new Date(data.startDate.date);
                $('#modalTitle').text("Editar sesión");
                $('#newSessionCustomer').val(data.customer).trigger('change');
                $('#newSessionCustomer').attr("disabled", true);
                $('#newSessionStartDate').val(startDateLocale.toLocaleDateString());
                $('#newSessionStartDate').attr("disabled", true);
                $('#newSessionStartTime').val(startDateLocale.toLocaleTimeString());
                $('#newSessionStartTime').attr("disabled", true);
                $('#newSessionDuration').val(data.duration);
                $('#newSessionDuration').attr("disabled", true);
                $('#sessionObservation').val(data.observation);
                $('#newSessionModal').modal('show');
                $('#scheduleDeleteBtn').attr('data-event-id', data.id);
            }
            else
                swal("Ooops!", "La carga del evento ha fallado!", "error");
        },
        error: function( jqXhr, textStatus, errorThrown ){
            swal("Ooops!", "La carga del evento ha fallado!", "error");
        }
    });
}

function createOrEditEvent(eventData = null) {
    var isNew = (eventData == null)? true : false;
    var actionUrl = (isNew)? '?entity=Schedule&action=createEvent' : '?entity=Schedule&action=updateEvent';
    var successMessage = (isNew)? 'La sesión se ha creado correctamente' : 'La sesión se ha actualizado correctamente';
    var errorMessage = (isNew)? "Algo ha fallado durante la creación inténtelo de nuevo y si el problema persiste póngase en contacto con el administrador!" : "Algo ha fallado durante la actualización inténtelo de nuevo y si el problema persiste póngase en contacto con el administrador!";

    var eventId = (isNew)? '' : eventData.className[0].replace("fwdss_session_", "");
    var customer = (isNew)? $('#newSessionCustomer').val() : eventData.customer;
    var startDate = $('#newSessionStartDate').val();
    var startTime = $('#newSessionStartTime').val();
    var duration = $('#newSessionDuration').val();
    var observation = $('#sessionObservation').val();
    var notify = $('#newSessionNotify').is(':checked')? 1 : 0;

    // First validate
    var validate = true;
    if(isNew) {
        if(customer == null || customer == ''){
            $('#newSessionCustomer').parent().addClass('has-error');
            validate = false;
        }
        else {
            $('#newSessionCustomer').parent().removeClass('has-error');
        }

        if(startDate == ''){
            $('#newSessionStartDate').parent().addClass('has-error');
        }
        else {
            $('#newSessionStartDate').parent().removeClass('has-error');
        }

        if(startTime == ''){
            $('#newSessionStartTime').parent().addClass('has-error');
            validate = false;
        }
        else {
            $('#newSessionStartTime').parent().removeClass('has-error');
        }

        if(duration == ''){
            $('#newSessionDuration').parent().addClass('has-error');
            validate = false;
        }
        else {
            $('#newSessionDuration').parent().removeClass('has-error');
        }
    }

    if(validate == false)
        return false;

    // First compose correctly date
    var dateArr = startDate.split("/");
    startDate = dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];

    $.ajax({
        type: 'POST',
        url: actionUrl,
        data: {
            eventId: eventId,
            notify: notify,
            customer: customer,
            duration: duration,
            start_date: startDate,
            start_time: startTime,
            observation: observation
        },
        success: function( data, textStatus, jQxhr ){
            $('#customFilter').trigger('change');
            swal({
                type: 'success',
                text: successMessage,
                title: 'Yuhuuu!!',
                timer: 1000,
            }).then(
                function() {
                },
                // Handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                        // Close modal
                        $('#newSessionModal').modal('hide');
                        $('#scheduleCalendar').fullCalendar('refetchEvents');
                        $('#modalTitle').text("Nueva sesión");
                        selectedSession = null;
                    }
                    if (dismiss === 'cancel')
                        console.log('Cancel by user');
                    // Clear new form
                    cleanScheduleForm();
                }
            );
        },
        error: function( jqXhr, textStatus, errorThrown ){
            swal("Ooops!", errorMessage, "error");
        }
    });
}

function cleanScheduleForm() {
    $('#newSessionCustomer').val(-1).trigger('change');
    $('#newSessionCustomer').attr("disabled", false);
    $('#newSessionStartDate').val('');
    $('#newSessionStartDate').attr("disabled", false);
    $('#newSessionStartTime').val('');
    $('#newSessionStartTime').attr("disabled", false);
    $('#newSessionDuration').val(60);
    $('#newSessionDuration').attr("disabled", false);
    $('#sessionObservation').val('');
    $('#scheduleDeleteBtn').attr('data-event-id', '');
}

$(function () {
    // Page schedule caldendar
    if($('#scheduleCalendar').length) {
        // Init customer new session on load page
        if($('#newSessionCustomer').length)
            $('#newSessionCustomer').val(-1).trigger('change');

        // Create schedule calendar
        $('#scheduleCalendar').fullCalendar({
            lang: 'es',
            height: "parent",
            header: {
                left: 'prev, next',
                center: 'title',
                right: 'month, agendaWeek, agendaDay'
            },
            defaultView: 'agendaWeek',
            lazyFetching: true,
            weekNumbers: false,
            timeFormat: 'H:mm',
            displayEventEnd: true,
            showNonCurrentDates: false,
            googleCalendarApiKey: 'AIzaSyC4_HQiHUsSnZx0INN5D-BmXNR9SzmNNDQ',
            eventSources: [
                {
                    id: 'fwdss_sessions',
                    type: 'POST',
                    url: Routing.generate('fullcalendar_loader'),
                    data: {
                        filter: getFilters()
                    },
                    error: function() {}
                },
                {
                    id: 'holidays',
                    googleCalendarId: 'lqknb35jmbq5qa2pdrbtm9hf64@group.calendar.google.com',
                    color: '#D50000'
                }
            ],
            eventLimit: true,
            views: {
                agenda: {
                    eventLimit: 3,
                    minTime: '06:00:00',
                    maxTime: '23:00:00',
                }
            },
            editable: true,
            eventDrop: function(event, delta, revertFunc) {
                updateEvent(event, revertFunc);
            },
            eventResize: function(event, delta, revertFunc) {
                updateEvent(event, revertFunc);
            },
            eventClick: function(event, jsEvent, view) {
                editEvent(event);
                $('#scheduleAddBtn').hide();
                $('#scheduleEditBtn').show();
                $('#scheduleDeleteBtn').show();
            },
            eventAfterAllRender: function(view) {
                var allevents = $('#scheduleCalendar').fullCalendar('clientEvents');
                var eventsOverlapping = [];
                // Get holiday events
                var holidayEvents = $.grep(allevents, function(event) {
                    return event.source.id === "holidays";
                });

                // If this period has holiday events then check if sessions are
                // scheduled on this days
                if(holidayEvents.length) {
                    // Check overlapping events
                    for (var i = 0; i < holidayEvents.length; i++) {
                        eventsOverlapping[i] = $.grep(allevents, function(event) {
                            return event.source.id === "fwdss_sessions" && moment(event.start).format('YYYY-MM-DD') === moment(holidayEvents[i].start).format('YYYY-MM-DD');
                        });
                    }
                    // Finally count overlapping total
                    var eventsOverlappingCount = 0;
                    for (var i = 0; i < eventsOverlapping.length; i++) {
                        eventsOverlappingCount += eventsOverlapping[i].length;
                    }
                    // Show or hide schedule alert
                    if(eventsOverlappingCount > 0) {
                        $('#scheduleCalendarAlertMsg').html('Existen <b>' + eventsOverlappingCount + '</b> sesiones planificadas algún día festivo.')
                        $('#scheduleCalendarAlert').fadeIn('slow', 'linear');
                    }
                    else {
                        $('#scheduleCalendarAlertMsg').html('')
                        $('#scheduleCalendarAlert').fadeOut('slow', 'linear');
                    }
                }
            }
        });

        // Generate month sessions
        $('#scheduleGenerateBtn').on('click', function(e) {
            e.preventDefault();
            var selectedMonth = $('#scheduleCalendar').fullCalendar('getDate').month();
            var selectedYear = $('#scheduleCalendar').fullCalendar('getDate').year();
            var customerSelected = $('#customFilter').val();
            // Send ajax
            swal({
                type: 'info',
                text: 'La generación de las sesiones puede tardar un poquito, espere tranquil@',
                title: '¿Estás segur@?',
                allowOutsideClick: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si estoy segur@!',
                confirmButtonColor: '#3085d6',
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    swal.showLoading();
                    return new Promise(function (resolve, reject) {
                        $.ajax({
                            type: 'GET',
                            url: '?entity=Schedule&action=generate',
                            data: {
                                month: selectedMonth,
                                year: selectedYear,
                                customer: customerSelected,
                            },
                            success: function( data, textStatus, jQxhr ){
                                swal({
                                    type: 'success',
                                    title: 'Creación completa!',
                                    timer: 1000
                                });
                                $('#scheduleCalendar').fullCalendar('refetchEvents');
                                $('#customFilter').trigger('change');
                            },
                            error: function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                                reject();
                            }
                        });
                    });
                }
            }).then(
                function() {},
                // Handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer')
                        console.log('I was closed by the timer')
                    if (dismiss === 'cancel')
                        console.log('Cancel by user');
                }
            );
        });

        $('#scheduleNewBtn').on('click', function(e) {
            cleanScheduleForm();
            $('#scheduleAddBtn').show();
            $('#scheduleEditBtn').hide();
            $('#scheduleDeleteBtn').hide();
        });

        // New session
        $('#scheduleAddBtn').on('click', function(e) {
            e.preventDefault();
            createOrEditEvent(null);
        });

        // Update session
        $('#scheduleEditBtn').on('click', function(e) {
            e.preventDefault();
            createOrEditEvent(selectedSession);
        });

        // Delete session
        $('#scheduleDeleteBtn').on('click', function(e) {
            deleteEvent(selectedSession);
        });

        // Send sessions to customers
        $('#scheduleSendBtn').on('click', function(e) {
            e.preventDefault();
            var selectedMonth = $('#scheduleCalendar').fullCalendar('getDate').month();
            var selectedYear = $('#scheduleCalendar').fullCalendar('getDate').year();
            var customerSelected = $('#customFilter').val();
            // Send ajax
            swal({
                type: 'info',
                title: '¿Estás segur@?',
                allowOutsideClick: false,
                text: 'La planificación de las sesiones puede tardar un poquito, espere tranquil@',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si estoy segur@!',
                confirmButtonColor: '#3085d6',
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    swal.showLoading();
                    return new Promise(function (resolve, reject) {
                        $.ajax({
                            type: 'POST',
                            url: '?entity=Schedule&action=send',
                            data: {
                                month: selectedMonth,
                                year: selectedYear,
                                customer: customerSelected,
                            },
                            success: function( data, textStatus, jQxhr ){
                                swal({
                                    type: 'success',
                                    title: 'Planificación completa!',
                                    timer: 1000
                                });
                            },
                            error: function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                                reject();
                            }
                        });
                    });
                }
            }).then(
                function() {},
                // Handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer')
                        console.log('I was closed by the timer')
                    if (dismiss === 'cancel')
                        console.log('Cancel by user');
                }
            );
        });

        // Clean month sessions
        $('#scheduleCleanBtn').on('click', function(e) {
            e.preventDefault();
            var selectedMonth = $('#scheduleCalendar').fullCalendar('getDate').month();
            var selectedYear = $('#scheduleCalendar').fullCalendar('getDate').year();
            var customerSelected = $('#customFilter').val();
            var numEventsToClean = 0;
            $.ajax({
                type: 'GET',
                url: '?entity=Schedule&action=clean',
                data: {
                    month: selectedMonth,
                    year: selectedYear,
                    customer: customerSelected,
                },
                success: function( data, textStatus, jQxhr ){
                    numEventsToClean = data.result;
                    swal({
                        type: 'warning',
                        text: 'Las ' + numEventsToClean + ' sesiones del mes serán borradas',
                        title: '¿Estás segur@?',
                        showCancelButton: true,
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Borrar',
                        showLoaderOnConfirm: true,
                        preConfirm: function () {
                            return new Promise(function (resolve, reject) {
                                $.ajax({
                                    type: 'GET',
                                    url: '?entity=Schedule&action=remove',
                                    data: {
                                        month: selectedMonth,
                                        year: selectedYear,
                                        customer: customerSelected,
                                    },
                                    success: function( data, textStatus, jQxhr ){
                                        if(data.result)
                                            resolve();
                                        else
                                            swal("Ooops!", "El borrado ha fallado!", "error");
                                    },
                                    error: function( jqXhr, textStatus, errorThrown ){
                                        swal("Ooops!", "El borrado ha fallado!", "error");
                                    }
                                })
                            })
                        },
                        allowOutsideClick: false
                    }).then(function() {
                        swal({
                            type: 'success',
                            title: 'Sesiones borradas!',
                            timer: 1000
                        });
                        $('#scheduleCalendar').fullCalendar('refetchEvents');
                    }).catch(function() {
                        console.log('Cancel by user');
                    });
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    swal("Ooops!", "Algo ha fallado durante el borrado!", "error");
                }
            });
        });
    }

    // Manage filters
    $('#customFilter').on('change', function(){
        // Prepare load events
        var events = {
            url: Routing.generate('fullcalendar_loader'),
            type: 'POST',
            data: {
                filter: getFilters()
            }
        }
        $('#scheduleCalendar').fullCalendar('removeEventSource', events);
        $('#scheduleCalendar').fullCalendar('addEventSource', events);

        if($(this).val()) {
            var selectedMonth = $('#scheduleCalendar').fullCalendar('getDate').month();
            var selectedYear = $('#scheduleCalendar').fullCalendar('getDate').year();
            $.ajax({
                type: 'POST',
                url: '?entity=Schedule&action=customerInfo',
                data: {
                    id: $(this).val(),
                    month: selectedMonth,
                    year: selectedYear,
                },
                success: function( data, textStatus, jQxhr ){
                    $('#customerInfo').removeClass('hide');
                    $('#customerNumSessions').text(data.sessions);
                    $('#customerRate').text(data.rate);
                }
            });
        }
        else {
            $('#customerInfo').addClass('hide');
            $('#customerNumSessions').text('');
            $('#customerRate').text('');
        }
    });
});
