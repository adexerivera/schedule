<?php

namespace AppBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Report.
 *
 * @author Adexe Rivera
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportRepository")
 *
 * @ORM\Table(name="report")
 */
class Report
{
    /**
     * The identifier of the report.
     *
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id = null;

    /**
     * The report message.
     *
     * @var string
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * The report image.
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $image;

    /**
     * The client for report.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reports")
     */
    protected $client;

    /**
     * The sender for report.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reports")
     */
    protected $sender;

    /**
     * The receiver for report.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reports")
     */
    protected $receiver;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $send;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $viewed;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \boolean
     * @ORM\Column(type="boolean")
     */
    private $erased;

    public function __construct()
    {
        $this->erased = false;
    }

    /**
     * Get the id of the report.
     * Return null if the report is new and not saved.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the message of the report.
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get the message of the report.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the image of the report.
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get the image of the report.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param User $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param User $sender
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
    }

    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param User $receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set the send date of the report.
     *
     * @return datetime $send
     */
    public function setSend($send)
    {
        $this->send = $send;
    }

    /**
     * Get the send date of the report.
     *
     * @return datetime
     */
    public function getSend()
    {
        return $this->send;
    }

    /**
     * Set the viewed date of the report.
     *
     * @return datetime $viewed
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
    }

    /**
     * Get the viewed date of the report.
     *
     * @return datetime
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Get the created date of the report.
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get the updated date of the report.
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set the erased of the report.
     *
     * @param boolean $erased
     */
    public function setErased($erased)
    {
        $this->erased = $erased;
    }

    /**
     * Get the erased of the report.
     *
     * @return boolean
     */
    public function getErased()
    {
        return $this->erased;
    }
}
