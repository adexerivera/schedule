<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\User;
use AppBundle\Entity\Token;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;

class ReportRemind extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('report:remind')
            ->setDescription('Report remind')
            ->addArgument(
                'platform',
                InputArgument::OPTIONAL,
                'Select platform android or ios'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        $platform = $input->getArgument('platform');

        $usersReported = $entityManager->getRepository(User::class)->findCustomerReported();
        $users = [];
        foreach ($usersReported as $user) {
            $users[] = $user->getId();
        }

        $tokens = $entityManager->getRepository(Token::class)->findTokensByUsers($users, $platform);

        $output->writeln("");
        $output->writeln("==========================================");
        $output->writeln(date("Y-m-d H:i:s") . ' - Report remind on ' . $platform);

        foreach ($tokens as $key => $token) {
            if(strtolower($token->getPlatform()) == strtolower($platform)) {
                if(strtolower($token->getPlatform()) === 'ios') {
                    $notification = new iOSMessage();
                    $notification->setAPSContentAvailable(1);
                } elseif (strtolower($token->getPlatform()) === 'android') {
                    $notification = new AndroidMessage();
                    $notification->setFCM(true);
                }
                $notification->setMessage("Es la hora del reporte!.\nEnvía tu reporte del día a Forward");
                $notification->setDeviceIdentifier($token->getToken());
                $notification->setData([
                    'message' => "Es la hora del reporte!.\nEnvía tu reporte del día a Forward"
                ]);
                $this->getContainer()->get('rms_push_notifications')->send($notification);

                $text = 'Notify to id ' . $token->getId() . ' with token ' . $token->getToken();
                $output->writeln($text);
            }
        }

        $output->writeln("==========================================");
        $output->writeln("");
    }
}
