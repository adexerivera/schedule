<?php

namespace AppBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Event.
 *
 * @author Adexe Rivera
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 *
 * @ORM\Table(name="event", indexes={@ORM\Index(name="search_idx", columns={"customer_id", "session_start", "session_end"})})
 */
class Event
{
    /**
     * The identifier of the event.
     *
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id = null;

    /**
     * The event name.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * The event description.
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * The customer for event.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="events")
     */
    protected $customer;

    /**
     * The trainer for event.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="events")
     */
    protected $trainer;

    /**
     * The workspace for event.
     *
     * @var Workspace
     * @ORM\ManyToOne(targetEntity="Workspace", inversedBy="events")
     */
    protected $workspace;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $session_start;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $session_end;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="events")
     */
    protected $createdBy;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="events")
     */
    protected $updatedBy;

    /**
     * @ORM\Column(name="type", type="string", columnDefinition="enum('anniversary', 'birthday', 'special', 'training')")
     */
    private $type;

    /**
     * @var boolean
     * @ORM\Column(name="all_day", type="boolean")
     */
    private $all_day;

    /**
     * @ORM\Column(name="source", type="string", columnDefinition="enum('system', 'manual')")
     */
    private $source;

    /**
     * @var boolean
     * @ORM\Column(name="attendance", type="boolean")
     */
    private $attendance;

    /**
     * The observation.
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $observation;

    public function __construct()
    {
        $this->type = 'training';
        $this->all_day = false;
        $this->source = 'system';
        $this->attendance = true;
    }

    /**
     * Get the id of the event.
     * Return null if the event is new and not saved.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the name of the event.
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the name of the event.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the description of the event.
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get the description of the event.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param User $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return User
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param User $trainer
     */
    public function setTrainer($trainer)
    {
        $this->trainer = $trainer;
    }

    /**
     * @return User
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * @param Workspace $workspace
     */
    public function setWorkspace($workspace)
    {
        $this->workspace = $workspace;
    }

    /**
     * @return Workspace
     */
    public function getWorkspace()
    {
        return $this->workspace;
    }

    /**
     * Set the session start date of the event.
     *
     * @return datetime $sessionStart
     */
    public function setSessionStart($sessionStart)
    {
        $this->session_start = $sessionStart;
    }

    /**
     * Get the session start date of the event.
     *
     * @return datetime
     */
    public function getSessionStart()
    {
        return $this->session_start;
    }

    /**
     * Set the session end date of the event.
     *
     * @return datetime $sessionEnd
     */
    public function setSessionEnd($sessionEnd)
    {
        $this->session_end = $sessionEnd;
    }

    /**
     * Get the session end date of the event.
     *
     * @return datetime
     */
    public function getSessionEnd()
    {
        return $this->session_end;
    }

    /**
     * Get the created date of the event.
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Get the updated date of the event.
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set session type
     *
     * @return type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get session type
     *
     * @return \enum
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the all day session.
     *
     * @param boolean $all_day
     */
    public function setAllDay($all_day)
    {
        $this->all_day = $all_day;
    }

    /**
     * Get the all day session.
     *
     * @return boolean
     */
    public function getAllDay()
    {
        return $this->all_day;
    }

    /**
     * Set session source
     *
     * @return source
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * Get session source
     *
     * @return \enum
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set the attendance of the user.
     *
     * @param boolean $attendance
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;
    }

    /**
     * Get the attendance of the user.
     *
     * @return boolean
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * Set the observations of the event.
     *
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * Get the observations of the event.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }
}
