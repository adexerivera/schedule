<?php

namespace AppBundle;

final class ScheduleEvents
{
    /**
     * This event occurs when a schedule is modified
     *
     * The event listener receives an
     * AppBundle\Event\ScheduleEvent instance.
     *
     * @var string
     */
    const UPDATED = 'schedule.updated';
}
