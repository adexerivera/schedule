<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * The user name.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * The user surname.
     *
     * @var string
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $surname;

    /**
     * The user phone.
     *
     * @var string
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $phone;

    /**
     * The user birthday.
     *
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday = null;

    /**
     * The user timetable.
     *
     * @var array
     * @ORM\Column(type="array", nullable=true)
     */
    private $timetable = null;

    /**
     * @var Event[]
     *
     * @ORM\OneToMany(targetEntity="Event", mappedBy="customer", cascade={"remove"})
     */
    private $workouts;

    /**
     * @var Event[]
     *
     * @ORM\OneToMany(targetEntity="Event", mappedBy="trainer", cascade={"remove"})
     */
    private $sessions;

    /**
     * The user active.
     *
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * The user rate.
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $rate;

    /**
     * The user forward anniversary.
     *
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $anniversary = null;

    /**
     * @var Token[]
     *
     * @ORM\OneToMany(targetEntity="Token", mappedBy="customer", cascade={"remove"})
     */
    private $tokens;

    /**
     * @var Message[]
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="customer", cascade={"remove"})
     */
    private $messages;

    /**
     * The user mobile app enabled.
     *
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $appEnabled;

    /**
     * The user active report.
     *
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $reported;

    public function __construct()
    {
        parent::__construct();

        $this->workouts = new ArrayCollection();
        $this->sessions = new ArrayCollection();
        $this->tokens = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->active = true;
        $this->appEnabled = false;
        $this->reported = true;
    }

    /**
     * Set the name of the user.
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the name of the user.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the surname of the user.
     *
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * Get the surname of the user.
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set the phone of the user.
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get the phone of the user.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the birthday of the user.
     *
     * @param string $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * Get the birthday of the user.
     *
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set the timetable of the user.
     *
     * @param array $timetable
     */
    public function setTimetable($timetable)
    {
        $this->timetable = $timetable;
    }

    /**
     * Get the timetable of the user.
     *
     * @return array
     */
    public function getTimetable()
    {
        return $this->timetable;
    }

    /**
     * @param Event[] $workouts
     */
    public function setWorkouts($workouts)
    {
        $this->workouts = $workouts;
    }

    /**
     * @return Event[]
     */
    public function getWorkouts()
    {
        return $this->workouts;
    }

    /**
     * @param Event[] $sessions
     */
    public function setSessions($sessions)
    {
        $this->sessions = $sessions;
    }

    /**
     * @return Event[]
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set the active of the user.
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get the active of the user.
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the anniversary of the user.
     *
     * @param string $anniversary
     */
    public function setAnniversary($anniversary)
    {
        $this->anniversary = $anniversary;
    }

    /**
     * Get the anniversary of the user.
     *
     * @return string
     */
    public function getAnniversary()
    {
        return $this->anniversary;
    }

    /**
     * Set the rate of the user.
     *
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * Get the rate of the user.
     *
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Get user age without params
     *
     * @return string
     */
    public function getAgeList()
    {
        $now = new \Datetime('now');
        return $this->getAge($now);
    }

    /**
     * Get user age
     *
     * @param date $actual Actual date to compare
     * @return string
     */
    public function getAge($actual)
    {
        $age = $this->getBirthday();
        if($age) {
            $difference = $actual->diff($age);
            return $difference->format('%y');
        }
        else {
            return "";
        }
    }

    /**
     * Get user anniversary age
     *
     * @param date $actual Actual date to compare
     * @return string
     */
    public function getAnniversaryAge($actual)
    {
        $age = $this->getAnniversary();
        if($age) {
            $difference = $actual->diff($age);
            return $difference->format('%y');
        }
        else {
            return "";
        }
    }

    /**
     * @param Token[] $tokens
     */
    public function setTokens($tokens)
    {
        $this->tokens = $tokens;
    }

    /**
     * @return Token[]
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @param Message[] $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set the app enabled of the user.
     *
     * @param boolean $appEnabled
     */
    public function setAppEnabled($appEnabled)
    {
        $this->appEnabled = $appEnabled;
    }

    /**
     * Get the app enabled of the user.
     *
     * @return boolean
     */
    public function getAppEnabled()
    {
        return $this->appEnabled;
    }

    /**
     * Set the reported of the user.
     *
     * @param boolean $appEnabled
     */
    public function setReported($reported)
    {
        $this->reported = $reported;
    }

    /**
     * Get the reported of the user.
     *
     * @return boolean
     */
    public function getReported()
    {
        return $this->reported;
    }

    /**
     * Get user full name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }
}
