<?php

namespace AppBundle\Controller\Admin;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use AppBundle\Entity\User;
use AppBundle\Entity\Event;
use AppBundle\Entity\Workspace;
use AppBundle\Entity\Diary;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Event\ScheduleEvent;
use AppBundle\ScheduleEvents;
use Symfony\Component\HttpFoundation\JsonResponse;

class ScheduleController extends BaseAdminController
{
    /**
     * @Route("/admin/schedule", name="schedule")
     */
    public function scheduleAction() {
        $customers = $this->getDoctrine()
            ->getRepository(User::class)
            ->findCustomer();
        return $this->render('easy_admin/schedule/schedule.html.twig', [
            'customers' => $customers,
        ]);
    }

    /**
     * @Route("/admin/schedule/generate", name="generate")
     */
    public function generateAction() {
        $em = $this->getDoctrine()->getManager();

        // First get params to prepare month dates and get customer selected
        $month = $this->request->get('month');
        $year = $this->request->get('year');
        $customerParam = $this->request->get('customer');

        // First day of the month.
        $iniDate =  strtotime(date('Y-m-01', strtotime(date($year . '-' . intval($month + 1) . '-1'))));
        // Last day of the month.
        $endDate =  strtotime(date('Y-m-t', strtotime(date($year . '-' . intval($month + 1) . '-1'))));

        // Prepare month days array
        $monhtDays = [];
        for($i = $iniDate; $i <= $endDate; $i += 86400){
            // Get week day
            $day = date('N', $i);
            switch ($day) {
                case 1:
                    $monhtDays['L'][] = date("Y-m-d", $i);
                    break;
                case 2:
                    $monhtDays['M'][] = date("Y-m-d", $i);
                    break;
                case 3:
                    $monhtDays['X'][] = date("Y-m-d", $i);
                    break;
                case 4:
                    $monhtDays['J'][] = date("Y-m-d", $i);
                    break;
                case 5:
                    $monhtDays['V'][] = date("Y-m-d", $i);
                    break;
                case 6:
                    $monhtDays['S'][] = date("Y-m-d", $i);
                    break;
                case 7:
                    $monhtDays['D'][] = date("Y-m-d", $i);
                    break;
            }
        }

        // Get customers if customer param is set then only generate this
        // customer sessions
        if($customerParam != '') {
            $customers = $this->getDoctrine()
                ->getRepository(User::class)
                ->findById($customerParam);
        }
        else {
            $customers = $this->getDoctrine()
                ->getRepository(User::class)
                ->findCustomer();
        }
        // Get workspace
        $workspace = $this->getDoctrine()
            ->getRepository(Workspace::class)
            ->findOneById(1);

        // Generate events for this month
        foreach ($customers as $key => $customer) {
            // First prepare training sessions start and end
            $timetables = $customer->getTimetable();
            if($timetables) {
                foreach ($timetables as $key => $timetable) {
                    $timetableDays = explode(',', $timetable['days']);
                    foreach ($timetableDays as $weekDay) {
                        if($weekDay == '') continue;
                        foreach ($monhtDays[$weekDay] as $realDay) {
                            $sessionStartStr = $realDay . ' ' . $timetable['hour'] . ':00';
                            $sessionStart = new \Datetime($sessionStartStr);
                            $sessionEndStr = date('Y-m-d H:i:s', strtotime($sessionStartStr) + 3600);
                            $sessionEnd = new \Datetime($sessionEndStr);

                            // Check if event already exist to prevent duplicate
                            $eventExists = $this->getDoctrine()
                            ->getRepository(Event::class)
                            ->existEvent($customer->getId(), $sessionStart, $sessionEnd, 'training');
                            if(!$eventExists) {
                                $event = new Event();
                                $event->setCustomer($customer);
                                $event->setWorkspace($workspace);
                                $event->setName('Sesión');
                                $event->setSessionStart($sessionStart);
                                $event->setSessionEnd($sessionEnd);
                                $event->setType('training');
                                $em->persist($event);
                                $em->flush();
                            }
                        }
                    }
                }
            }
            // Now create birthday events
            if($customer->getBirthday() && $month + 1 == $customer->getBirthday()->format('m')) {
                $birthdayEventDate = new \Datetime($year . '-' . $customer->getBirthday()->format('m') . '-' . $customer->getBirthday()->format('d') . ' 00:00:00');
                // Check if event already exist to prevent duplicate
                $eventExists = $this->getDoctrine()
                    ->getRepository(Event::class)
                    ->existEvent($customer->getId(), $birthdayEventDate, $birthdayEventDate, 'birthday');
                if(!$eventExists) {
                    $event = new Event();
                    $event->setCustomer($customer);
                    $event->setName('Cumpleaños');
                    $event->setSessionStart($birthdayEventDate);
                    $event->setSessionEnd($birthdayEventDate);
                    $event->setType('birthday');
                    $event->setAllDay(true);
                    $em->persist($event);
                    $em->flush();
                }
            }
            // Finally create anniversary events
            if($customer->getAnniversary() && $month + 1 == $customer->getAnniversary()->format('m') && $year > $customer->getAnniversary()->format('Y')) {
                $anniversaryEventDate = new \Datetime($year . '-' . $customer->getAnniversary()->format('m') . '-' . $customer->getAnniversary()->format('d') . ' 00:00:00');
                // Check if event already exist to prevent duplicate
                $eventExists = $this->getDoctrine()
                    ->getRepository(Event::class)
                    ->existEvent($customer->getId(), $anniversaryEventDate, $anniversaryEventDate, 'anniversary');
                if(!$eventExists) {
                    $event = new Event();
                    $event->setCustomer($customer);
                    $event->setName('Aniversario');
                    $event->setSessionStart($anniversaryEventDate);
                    $event->setSessionEnd($anniversaryEventDate);
                    $event->setType('anniversary');
                    $event->setAllDay(true);
                    $em->persist($event);
                    $em->flush();
                }
            }
        }
        return $this->json(array('success' => '1'));
    }

    /**
     * @Route("/admin/schedule/clean", name="clean")
     */
    public function cleanAction() {
        $em = $this->getDoctrine()->getManager();

        // First get params to prepare month dates
        $month = $this->request->get('month');
        $year = $this->request->get('year');
        $customerParam = $this->request->get('customer');

        // First day of the month.
        $iniDate =  date('Y-m-01 00:00:00', strtotime(date($year . '-' . intval($month + 1) . '-1')));
        // Last day of the month.
        $endDate =  date('Y-m-t 23:59:59', strtotime(date($year . '-' . intval($month + 1) . '-1')));

        // Individual customer
        if($customerParam != '') {
            $events = $this->getDoctrine()
                ->getRepository(Event::class)
                ->eventsCustomerMonth($customerParam, $iniDate, $endDate, 'training');
        }
        else {
            $events = $this->getDoctrine()
                ->getRepository(Event::class)
                ->eventsMonth($iniDate, $endDate);
        }

        return $this->json(array('result' => count($events)));
    }

    public function removeAction() {
        $em = $this->getDoctrine()->getManager();

        // First get params to prepare month dates
        $month = $this->request->get('month');
        $year = $this->request->get('year');
        $customerParam = $this->request->get('customer');

        // First day of the month.
        $iniDate =  date('Y-m-01 00:00:00', strtotime(date($year . '-' . intval($month + 1) . '-1')));
        // Last day of the month.
        $endDate =  date('Y-m-t 23:59:59', strtotime(date($year . '-' . intval($month + 1) . '-1')));

        // Individual customer
        if($customerParam != '') {
            $events = $this->getDoctrine()
                ->getRepository(Event::class)
                ->eventsCustomerMonth($customerParam, $iniDate, $endDate, 'training');
        }
        else {
            $events = $this->getDoctrine()
                ->getRepository(Event::class)
                ->eventsMonth($iniDate, $endDate);
        }

        // First delete events
        foreach ($events as $event) {
            $em->remove($event);
        }
        $em->flush();

        // Individual customer
        if($customerParam != '') {
            $diaries = $this->getDoctrine()
                ->getRepository(Diary::class)
                ->customerDiary($customerParam, intval($month + 1), $year);
        }
        else {
            $diaries = $this->getDoctrine()
                ->getRepository(Diary::class)
                ->diariesMonth(intval($month + 1), $year);
        }

        // Now delete diary
        foreach ($diaries as $diary) {
            $em->remove($diary);
        }
        $em->flush();
        return $this->json(array('result' => true));
    }

    /**
     * Get price for session count
     * @param  [integer] $sessions Session numbers
     * @return [string]            Price
     */
    public function getPrice($sessions) {
        // Prices
        $prices = [
            4 => '120',
            8 => '200',
            12 => '250',
            16 => '300',
        ];
        $closest = null;
        foreach ($prices as $key => $price) {
            if ($closest === null || abs($sessions - $closest) > abs($key - $sessions)) {
                $closest = $key;
            }
       }
       return $prices[$closest];
    }

    /**
     * @Route("/admin/schedule/send", name="send")
     */
    public function sendAction() {
        $em = $this->getDoctrine()->getManager();

        // First get params to prepare month dates
        $month = $this->request->get('month');
        $year = $this->request->get('year');
        $spanishMonths = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $monthName = $spanishMonths[$month];
        $iniDate =  date('Y-m-01 00:00:00', strtotime(date($year . '-' . intval($month + 1) . '-1')));
        $endDate =  date('Y-m-t 23:59:59', strtotime(date($year . '-' . intval($month + 1) . '-1')));
        // Get if customer is selected
        $customerParam = $this->request->get('customer');

        // Prepare filename
        $filename = sprintf(
            'Sesiones_%s.pdf',
            date('m_Y', strtotime(date($year . '-' . intval($month + 1) . '-1')))
        );


        // Send to all customers or unique customer is customer param set
        if($customerParam != '') {
            $customers = $this->getDoctrine()
                ->getRepository(User::class)
                ->findById($customerParam);
        }
        else {
            $customers = $this->getDoctrine()
                ->getRepository(User::class)
                ->findCustomer();
        }
        foreach ($customers as $customer) {
            // Create diary if not exists or force if customer param is selected
            $diaryExists = $this->getDoctrine()
                ->getRepository(Diary::class)
                ->existDiary($customer->getId(), $month + 1, $year);

            if(!$diaryExists) {
                // Get events
                $events = $this->getDoctrine()
                    ->getRepository(Event::class)
                    ->eventsCustomerMonth($customer->getId(), $iniDate, $endDate, 'training');

                if(count($events)) {
                    // Calculate rate if customer rate it's 0 or null
                    $rate = $customer->getRate();
                    if($rate == NULL)
                        $rate = $this->getPrice(count($events));
                    $diary = new Diary();
                    $diary->setCustomer($customer);
                    $diary->setUsername($customer->getUsername());
                    $diary->setMonth($month + 1);
                    $diary->setYear($year);
                    $diary->setSessions(count($events));
                    $diary->setRate($rate);
                    $em->persist($diary);
                    $em->flush();
                }
            }
            else {
                // Update diary if customer param is selected
                if($customerParam != '') {
                    $diary = $this->getDoctrine()
                        ->getRepository(Diary::class)
                        ->getDiary($customer->getId(), $month + 1, $year);

                    // Get events
                    $events = $this->getDoctrine()
                        ->getRepository(Event::class)
                        ->eventsCustomerMonth($customer->getId(), $iniDate, $endDate, 'training');

                    if(count($events)) {
                        // Calculate rate if customer rate it's 0 or null
                        $rate = $customer->getRate();
                        if($rate == NULL)
                            $rate = $this->getPrice(count($events));
                        $diary->setSessions(count($events));
                        $diary->setRate($rate);
                        $diary->setReplanned(true);
                        $diary->setSent(false);
                        $em->persist($diary);
                        $em->flush();
                    }
                }
            }
        }
        return $this->json(array('result' => true));
    }

    /**
     * @Route("/admin/schedule/udpate", name="update")
     */
    public function updateAction() {
        // First get params
        $eventId = $this->request->get('eventId');
        $sessionStart = \DateTime::createFromFormat('Y-m-d H:i:s', $this->request->get('start'));
        $sessionEnd = \DateTime::createFromFormat('Y-m-d H:i:s', $this->request->get('end'));
        $notify = $this->request->get('notify');
        // Get event
        $em = $this->getDoctrine()->getEntityManager();
        $event = $this->getDoctrine()
            ->getRepository(Event::class)
            ->find($eventId);
        // Get previous date
        $eventPreviousStart = $event->getSessionStart();
        // Get current user
        $user = $this->getUser();
        // Update event data
        $hasChange = false;
        if($event->getSessionStart() != $sessionStart) {
            $event->setSessionStart($sessionStart);
            $hasChange = true;
        }
        if($event->getSessionEnd() != $sessionEnd) {
            $event->setSessionEnd($sessionEnd);
            $hasChange = true;
        }

        // Save if has change
        if($hasChange) {
            $event->setUpdatedBy($user);
            $em->persist($event);
            $em->flush();

            // Dispatch Event
            if($notify) {
                $scheduleEvent = new ScheduleEvent($event, $eventPreviousStart, 'update', 'web');
                $scheduleEvent = $this->container->get('event_dispatcher')->dispatch(ScheduleEvents::UPDATED, $scheduleEvent);
            }
        }
        return $this->json(array('result' => true));
    }

    /**
     * @Route("/admin/schedule/udpateEvent", name="updateEvent")
     */
    public function updateEventAction() {
        // First get params
        $eventId = $this->request->get('eventId');
        $notify = $this->request->get('notify');
        $observation = $this->request->get('observation');

        // Get event
        $em = $this->getDoctrine()->getEntityManager();
        $event = $this->getDoctrine()
            ->getRepository(Event::class)
            ->find($eventId);

        // Get current user
        $user = $this->getUser();
        // Update event data
        $hasChange = false;
        if($event->getObservation() != $observation) {
            $event->setObservation($observation);
            $hasChange = true;
        }

        // Save if has change
        if($hasChange) {
            $event->setUpdatedBy($user);
            $em->persist($event);
            $em->flush();
        }
        return $this->json(array('result' => true));
    }

    /**
     * @Route("/admin/schedule/createEvent", name="createEvent")
     */
    public function createEventAction() {
        // First get params
        $customerId = $this->request->get('customer');
        $startDate = $this->request->get('start_date');
        $startTime = $this->request->get('start_time');
        $duration = $this->request->get('duration');
        $notify = $this->request->get('notify');
        $observation = $this->request->get('observation');

        // Prepare session start and end
        $sessionStartStr = $startDate . ' ' . $startTime . ':00';
        $sessionStart = new \Datetime($sessionStartStr);
        $sessionEndStr = date('Y-m-d H:i:s', strtotime($sessionStartStr) + (60*$duration));
        $sessionEnd = new \Datetime($sessionEndStr);

        // Get customer and workspace
        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($customerId);
        $workspace = $this->getDoctrine()
            ->getRepository(Workspace::class)
            ->findOneById(1);
        // Get current user
        $user = $this->getUser();

        // Check if event already exist to prevent duplicate
        $eventExists = $this->getDoctrine()
            ->getRepository(Event::class)
            ->existEvent($customer->getId(), $sessionStart, $sessionEnd, 'training');
        if(!$eventExists) {
            $em = $this->getDoctrine()->getEntityManager();
            $event = new Event();
            $event->setCustomer($customer);
            $event->setWorkspace($workspace);
            $event->setName('Sesión');
            $event->setSessionStart($sessionStart);
            $event->setSessionEnd($sessionEnd);
            $event->setSource('manual');
            $event->setCreatedBy($user);
            $event->setObservation($observation);
            $em->persist($event);
            $em->flush();

            // Dispatch Event
            if($notify) {
                $scheduleEvent = new ScheduleEvent($event, $sessionStart, 'new', 'web');
                $scheduleEvent = $this->container->get('event_dispatcher')->dispatch(ScheduleEvents::UPDATED, $scheduleEvent);
            }
        }
        return $this->json(array('result' => true));
    }

    /**
     * @Route("/admin/schedule/getEvent", name="getEvent")
     */
    public function getEventAction() {
        // First get params
        $eventId = $this->request->get('eventId');

        // Get event
        $em = $this->getDoctrine()->getEntityManager();
        $event = $this->getDoctrine()
            ->getRepository(Event::class)
            ->find($eventId);

        $duration = abs(($event->getSessionStart()->getTimestamp() - $event->getSessionEnd()->getTimestamp())/60);

        return new JsonResponse([
            'id' => $event->getId(),
            'customer' => $event->getCustomer()->getId(),
            'startDate' => $event->getSessionStart(),
            'startTime' => $event->getSessionStart(),
            'duration' => $duration,
            'observation' => $event->getObservation()
        ]);
    }

    /**
     * @Route("/admin/schedule/deleteEvent", name="deleteEvent")
     */
    public function deleteEventAction() {
        // First get params
        $eventId = $this->request->get('eventId');
        $notify = $this->request->get('notify');
        // Get event
        $em = $this->getDoctrine()->getEntityManager();
        $event = $this->getDoctrine()
            ->getRepository(Event::class)
            ->find($eventId);
        // Get previous date
        $eventPreviousStart = $event->getSessionStart();
        // Delete event
        try {
            $em->remove($event);
            $em->flush();
            // Dispatch Event
            if($notify) {
                $scheduleEvent = new ScheduleEvent($event, $eventPreviousStart, 'delete', 'web');
                $scheduleEvent = $this->container->get('event_dispatcher')->dispatch(ScheduleEvents::UPDATED, $scheduleEvent);
            }
            return $this->json(array('result' => true));
        } catch (ForeignKeyConstraintViolationException $e) {
            return $this->json(array('result' => true));
        }
    }

    /**
     * @Route("/admin/schedule/customerInfo", name="customerInfo")
     */
    public function customerInfoAction() {
        // First get params
        $id = $this->request->get('id');
        $month = $this->request->get('month');
        $year = $this->request->get('year');
        $iniDate =  date('Y-m-01 00:00:00', strtotime(date($year . '-' . intval($month + 1) . '-1')));
        $endDate =  date('Y-m-t 23:59:59', strtotime(date($year . '-' . intval($month + 1) . '-1')));
        // Get customer
        $customer = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneById($id);
        // Get events
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->eventsCustomerMonth($customer->getId(), $iniDate, $endDate, 'training');
        // Get user diary rate
        $diaryExists = $this->getDoctrine()
            ->getRepository(Diary::class)
            ->existDiary($customer->getId(), $month + 1, $year);
        if($diaryExists) {
            $diary = $this->getDoctrine()
                ->getRepository(Diary::class)
                ->customerDiary($customer, intval($month + 1), $year);
            $rate = $diary[0]->getRate();
        }
        else {
            $rate = $customer->getRate();
        }

        return $this->json(['rate' => $rate, 'sessions' => count($events)]);
    }
}
