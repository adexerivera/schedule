<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\User as User;
use AppBundle\Entity\Report as Report;

class ReportEvent extends Event
{
    private $report;

    private $sender;

    private $receiver;

    /**
     * Create report event for events
     * @param User    $sender       Report sender
     * @param User    $receiver     Report receiver
     */

    public function __construct(Report $report, User $sender, User $receiver) {
        $this->report = $report;
        $this->sender = $sender;
        $this->receiver = $receiver;
    }

    public function getReport(){
        return $this->report;
    }

    public function getSender(){
        return $this->sender;
    }

    public function getReceiver(){
        return $this->receiver;
    }
}
