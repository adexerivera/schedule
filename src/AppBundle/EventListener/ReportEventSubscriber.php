<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Event\ReportEvent;
use AppBundle\ReportEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Token;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Entity\Diary;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;

class ReportEventSubscriber implements EventSubscriberInterface
{
    private $entityManager;
    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container) {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents() {
        return array(
            ReportEvents::SEND => 'onReportSend',
        );
    }

    /**
     * On report updated
     * @param  ReportEvent $report Report
     */
    public function onReportSend(ReportEvent $report) {
        $report = $report->getReport();
        $sender = $report->getSender();
        $receiver = $report->getReceiver();

        // Get trainers
        $trainers = $this->entityManager->getRepository(User::class)->findTrainer();
        $users = [];
        if($sender->hasRole('ROLE_ADMIN')) {
            // Get receiver
            $users[] = $receiver->getId();
            // Get other trainers
            foreach ($trainers as $trainer) {
                if($trainer->getId() != $sender->getId()) {
                    $users[] = $trainer->getId();
                }
            }
        }
        else {
            // All trainers
            foreach ($trainers as $trainer) {
                $users[] = $trainer->getId();
            }
        }
        // Get active tokens for users
        $tokens = $this->entityManager->getRepository(Token::class)->findTokensByUsers($users);

        $senderUsername = $sender->getUsername();
        if($senderUsername == 'rdearmas')
            $senderUsername = 'Ruymán';

        $notificationMessage = '';
        $sendNotificationReport = new \DateTime();
        foreach ($tokens as $key => $token) {
            if(strtolower($token->getPlatform()) === 'ios') {
                $notification = new iOSMessage();
                $notification->setAPSContentAvailable(1);
            } elseif (strtolower($token->getPlatform()) === 'android') {
                $notification = new AndroidMessage();
                $notification->setFCM(true);
            }

            $notificationMessage = $senderUsername . ' te ha enviado un mensaje el ' . strftime('%d/%m/%Y a las %H:%M', $sendNotificationReport->getTimestamp());

            $notification->setMessage($notificationMessage);
            $notification->setDeviceIdentifier($token->getToken());
            $notification->setData([
                'type' => 'report',
                'message' => $notificationMessage,
                'report' => [
                    'id' => $report->getId(),
                    'sender_id' => $sender->getId(),
                    'sender_name' => $senderUsername,
                    'sender_admin' => $sender->hasRole('ROLE_ADMIN')? 1 : 0,
                    'sender_avatar' => $sender->hasRole('ROLE_ADMIN')? 'assets/imgs/logo.png' : strtoupper(substr($sender->getUsername(), 0)),
                    'receiver_id' => $receiver->getId(),
                    'receiver_name' => $receiver->getUsername(),
                    'receiver_admin' => $receiver->hasRole('ROLE_ADMIN')? 1 : 0,
                    'receiver_avatar' => $receiver->hasRole('ROLE_ADMIN')? 'assets/imgs/logo.png' : strtoupper(substr($receiver->getUsername(), 0)),
                    'send' => date_format($report->getSend(),"c"),
                    'message' => $report->getMessage(),
                    'image' => ($report->getImage())? 1 : 0,
                    'status' => 'success',
                    'viewed' => $report->getViewed()
                ]
            ]);
            $this->container->get('rms_push_notifications')->send($notification);
        }

        return new JsonResponse([
            'result' => true,
        ]);

    }
}
