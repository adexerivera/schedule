<?php

namespace AppBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Message.
 *
 * @author Adexe Rivera
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 *
 * @ORM\Table(name="message")
 */
class Message
{
    /**
     * The identifier of the message.
     *
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id = null;

    /**
     * The message title.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    private $title;

    /**
     * The message body.
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $body;

    /**
     * The customer for message.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     */
    protected $customer;

    /**
     * @ORM\Column(name="type", type="string", columnDefinition="enum('mail', 'notification', 'difussion', 'authorization')")
     */
    private $type;

    /**
     * @ORM\Column(name="source", type="string", columnDefinition="enum('system', 'manual')")
     */
    private $source;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $send;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $viewed;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * The notification active.
     *
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * The difussion.
     *
     * @var Difussion
     * @ORM\ManyToOne(targetEntity="Difussion", inversedBy="messages")
     */
    protected $difussion;

    /**
     * The authorization acceptance.
     *
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accepted;

    /**
     * The authorization token.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorization_token;

    public function __construct()
    {
        $this->type = 'notification';
        $this->source = 'system';
        $this->active = true;
    }

    /**
     * Get the id of the message.
     * Return null if the message is new and not saved.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the title of the message.
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get the title of the message.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the body of the message.
     *
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get the body of the message.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param User $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return User
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set the send date of the message.
     *
     * @return datetime $send
     */
    public function setSend($send)
    {
        $this->send = $send;
    }

    /**
     * Get the send date of the message.
     *
     * @return datetime
     */
    public function getSend()
    {
        return $this->send;
    }

    /**
     * Set the viewed date of the message.
     *
     * @return datetime $viewed
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
    }

    /**
     * Get the viewed date of the message.
     *
     * @return datetime
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Get the created date of the message.
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get the updated date of the message.
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set session type
     *
     * @return type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get session type
     *
     * @return \enum
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set session source
     *
     * @return source
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * Get session source
     *
     * @return \enum
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set the active of the notification.
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get the active of the notification.
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param Difussion $difussion
     */
    public function setDifussion($difussion)
    {
        $this->difussion = $difussion;
    }

    /**
     * @return Difussion
     */
    public function getDifussion()
    {
        return $this->difussion;
    }

    /**
     * Set the accepted of the authorization.
     *
     * @param boolean $accepted
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }

    /**
     * Get the accepted of the authorization.
     *
     * @return boolean
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * Set the authorization token.
     *
     * @param string $authorization_token
     */
    public function setAuthorizationToken($authorization_token)
    {
        $this->authorization_token = $authorization_token;
    }

    /**
     * Get the authorization token.
     *
     * @return string
     */
    public function getAuthorizationToken()
    {
        return $this->authorization_token;
    }
}
