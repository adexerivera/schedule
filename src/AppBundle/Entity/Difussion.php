<?php

namespace AppBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Difussion.
 *
 * @author Adexe Rivera
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DifussionRepository")
 *
 * @ORM\Table(name="difussion")
 */
class Difussion
{
    /**
     * The identifier of the difussion.
     *
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id = null;

    /**
     * The difussion title.
     *
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    private $title;

    /**
     * The difussion body.
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $body;

    /**
     * @ORM\Column(name="type", type="string", columnDefinition="enum('mail', 'notification', 'authorization')")
     */
    private $type;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $send;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * The difussion active.
     *
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * The difussion count.
     *
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $count;

    public function __construct()
    {
        $this->type = 'notification';
        $this->active = true;
    }

    /**
     * Get the id of the difussion.
     * Return null if the difussion is new and not saved.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the title of the difussion.
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get the title of the difussion.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the body of the difussion.
     *
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get the body of the difussion.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set the send date of the difussion.
     *
     * @return datetime $send
     */
    public function setSend($send)
    {
        $this->send = $send;
    }

    /**
     * Get the send date of the difussion.
     *
     * @return datetime
     */
    public function getSend()
    {
        return $this->send;
    }

    /**
     * Get the created date of the difussion.
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get the updated date of the difussion.
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set session type
     *
     * @return type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get session type
     *
     * @return \enum
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the active of the difussion.
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get the active of the difussion.
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the count of the difussion.
     *
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * Get the count of the difussion.
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}
