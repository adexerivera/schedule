/**
 * Diary javascript functionalities
 *
 * @author Adexe Rivera (adexerivera@gmail.com)
 */
$(function () {
    // Send sessions to customers
    $('#diarySendBtn').on('click', function(e) {
        e.preventDefault();
        var selectedMonth = $('#selectedMonth').val();
        var selectedYear = $('#selectedYear').val();
        var validate = true;

        if(selectedMonth == ''){
            $('#selectedMonth').parent().addClass('has-error');
            validate = false;
        }
        else {
            $('#selectedMonth').parent().removeClass('has-error');
        }

        if(selectedYear == ''){
            $('#selectedYear').parent().addClass('has-error');
            validate = false;
        }
        else {
            $('#selectedYear').parent().removeClass('has-error');
        }

        if(validate == false)
            return false;

        // Send ajax
        swal({
            type: 'info',
            text: 'El envío de las sessiones puede tardar un poquito, espere tranquil@',
            title: 'Procesando ...',
            allowOutsideClick: false,
            onOpen: function () {
                swal.showLoading();
                return new Promise(function (resolve, reject) {
                    $.ajax({
                        type: 'POST',
                        url: '?entity=Diary&action=send',
                        data: {
                            id: 0,
                            month: selectedMonth,
                            year: selectedYear,
                        },
                        success: function( data, textStatus, jQxhr ){
                            swal({
                                type: 'success',
                                title: 'Envío completo!',
                                timer: 1000
                            });
                            window.location.reload();
                        },
                        error: function( jqXhr, textStatus, errorThrown ){
                            console.log( errorThrown );
                            reject();
                        }
                    });
                });
            }
        }).then(
            function() {},
            // Handling the promise rejection
            function (dismiss) {
                if (dismiss === 'timer')
                    console.log('I was closed by the timer')
                if (dismiss === 'cancel')
                    console.log('Cancel by user');
            }
        );
    });
});
