<?php

namespace AppBundle;

final class ReportEvents
{
    /**
     * This event occurs when a report is send
     *
     * The event listener receives an
     * AppBundle\Event\ReportEvent instance.
     *
     * @var string
     */
    const SEND = 'report.send';
}
